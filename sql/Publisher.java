
public class Publisher {

	private String publisher_code;
	private String publisher_name;
	private String city;

	public Publisher(String publisher_code, String publisher_name, String city) {
		this.publisher_code = publisher_code;
		this.publisher_name = publisher_name;
		this.city = city;
	}
	
	public String toString() {
		return this.publisher_name;
	}


}
