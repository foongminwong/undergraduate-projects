
/**
 * Author: Foong Min Wong
 * 
 * 
 * Program: SearchByPublisherPanel.java 
 * 
 * **/

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchByPublisherPanel extends JPanel {

	private JComboBox<Book> bookCombo;
	private JTextField authorSelection;
	// tried JList for branchPanel but it didn't work very well
	private JPanel branchPanel;
	private JTextField price;

	private HenryDAO dao;
	private boolean flag;
	private List<Book> bookList;

	/** SearchByPublisherPanel Constructor **/
	public SearchByPublisherPanel(HenryDAO dao) {
		this.dao = dao;
		flag = false;
		authorSelection = new JTextField();
		price = new JTextField();

		// Set background color
		Color color = new Color(227, 178, 168);
		setBackground(color);

		// Adds the list of publisher names to the box
		List<String> publisherList = dao.getPublisherList();
		JComboBox<String> publisherCombo = new JComboBox<>(new Vector<>(publisherList));
		publisherCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox pubCombo = (JComboBox) e.getSource();
				// cast to String
				String publisher = (String) pubCombo.getSelectedItem();

				bookList = dao.getPublisherBookData(publisher);
				DefaultComboBoxModel<Book> bookComboBoxModel = new DefaultComboBoxModel<Book>(
						new Vector<Book>(bookList));
				bookCombo.setModel(bookComboBoxModel);
				selectedBookInfo(bookList.get(0));

			}
		});

		// add publisherCombo to the panel
		this.add(publisherCombo);

		bookCombo = new JComboBox<Book>();
		bookCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox bCombo = (JComboBox) e.getSource();
				// cast it to Book
				Book book = (Book) bCombo.getSelectedItem();
				selectedBookInfo(book);
			}
		});
		// add bookCombo to the panel
		this.add(bookCombo);

		authorSelection.setEditable(false);
		price.setEditable(false);

		bookList = dao.getPublisherBookData(publisherList.get(0));

		this.add(authorSelection);
		this.add(price);

		DefaultComboBoxModel<Book> bookComboBoxModel = new DefaultComboBoxModel<Book>(new Vector<Book>(bookList));
		bookCombo.setModel(bookComboBoxModel);
		selectedBookInfo(bookList.get(0));

		flag = true;
	}

	/** Set book info once a book is selected **/
	private void selectedBookInfo(Book book) {

		price.setText("$ " + book.getPrice());

		if (flag) {
			this.remove(branchPanel);
		}

		Author a = book.getAuthor();
		authorSelection.setText(a.getAuthorFirst() + "   " + a.getAuthorLast());

		if (true) {
			int branchSize = book.getBranch().size();
			branchPanel = new JPanel(new GridLayout(branchSize, 3));

			for (Branch branch : book.getBranch()) {
				branchPanel.add(new JLabel(branch.getName()));
				branchPanel.add(new JLabel(branch.getcopiesOfBooks() + ""));
			}
			this.add(branchPanel);
			this.revalidate();
			this.repaint();
		}

	}
}
