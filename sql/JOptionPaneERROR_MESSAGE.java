
/**
 * Author: Foong Min Wong
 * Program: JOptionPaneERROR_MESSAG.java
 * 
 * To pop out error message
 * 
 * **/
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class JOptionPaneERROR_MESSAGE {

	public static void main(String[] args) {
		final JPanel panel = new JPanel();

		JOptionPane.showMessageDialog(panel, "Book not available for this author! Please select other authors.",
				"Error", JOptionPane.ERROR_MESSAGE);

	}
}