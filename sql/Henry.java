
/**
 * Author: Foong Min Wong
 * Program: Henry.java (the main program)
 * 
 * **/

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class Henry extends JPanel {

	private JPanel searchByAuthorPanel;
	private JPanel searchByCategoryPanel;
	private JPanel searchByPublisherPanel;

	public Henry() throws ClassNotFoundException {
		HenryDAO dao = new HenryDAO();

		Color color = new Color(179, 228, 117);
		// Create a frame of Henry
		JFrame frame = new JFrame("Henry");

		// Create main panel
		JPanel main = new JPanel();

		// setLayout Border Layout
		main.setLayout(new BorderLayout());

		// Initialize the three panels
		searchByAuthorPanel = new SearchByAuthorPanel(dao);
		searchByCategoryPanel = new SearchByCategoryPanel(dao);
		searchByPublisherPanel = new SearchByPublisherPanel(dao);

		// Create a tabbed pane component
		JTabbedPane tabbedPane = new JTabbedPane();

		// Add the tabs
		tabbedPane.addTab("Search by Author", searchByAuthorPanel);
		tabbedPane.addTab("Search by Category", searchByCategoryPanel);
		tabbedPane.addTab("Search by Publisher", searchByPublisherPanel);

		// add the tabbed pane component to the panel
		add(tabbedPane, BorderLayout.CENTER);

		main.add(tabbedPane, BorderLayout.CENTER);

		frame.setContentPane(main);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.pack();
		frame.setLocationRelativeTo(null);

		// Set the size of the frame
		frame.setSize(632, 338);
		// Display the window.
		frame.setVisible(true);

	}

	/**
	 * Main method to invoke Henry()
	 * 
	 * @throws ClassNotFoundException
	 * 
	 **/
	public static void main(String[] args) throws ClassNotFoundException {
		new Henry();
	}

}
