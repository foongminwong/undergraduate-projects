
/**
 * Author: Foong Min Wong
 * Program: Author.java
 * 
 * **/
import java.util.ArrayList;
import java.util.List;

public class Author {

	private List<Book> bookList;
	private String author_first;
	private String author_last;

	/** Author Constructor **/
	public Author(String author_first, String author_last) {
		this.author_first = author_first;
		this.author_last = author_last;
		bookList = new ArrayList<>();
	}

	/**
	 * Get author's firstname
	 * 
	 * @return author_first
	 **/
	public String getAuthorFirst() {
		return author_first;
	}

	/**
	 * Get author's lastname
	 * 
	 * @return author_last
	 **/
	public String getAuthorLast() {
		return author_last;
	}

	/**
	 * Add given book to a booklist
	 * 
	 * 
	 **/
	public void addBook(Book book) {

		boolean flag = false;
		int bookCount = 0;
		book.setAuthor(this);

		if (bookList.size() != 0) {

			while (!(flag || bookList.size() <= bookCount)) {

				if (bookList.get(bookCount).equals(book)) {

					bookList.get(bookCount).addBranch(book.getBranch().get(0));
					flag = true;

				} else {

					++bookCount;
				}
			}

			if (bookCount == bookList.size()) {
				book.setAuthor(this);
				bookList.add(book);
			}
		} else {
			book.setAuthor(this);
			bookList.add(book);
		}

	}

	/**
	 * Get a list of books
	 * 
	 * @return bookList
	 **/
	public List<Book> getBookList() {
		return bookList;
	}

	/* Overrides Java toString method **/
	public String toString() {
		return author_first + " " + author_last;
	}

}
