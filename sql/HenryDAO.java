import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Author: Foong Min Wong Program: HenryDAO.java
 * 
 **/

public class HenryDAO {

	// Conenction, Statement, ResultSet
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	private String DB_URL;
	private String USER;;
	private String PASS;

	/** HenryDAO Constructor **/
	public HenryDAO() throws ClassNotFoundException {

		// Open a connection to the database
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cs260", "root", "12345");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Retrieve author book data
	 * 
	 * @param author a specified given author
	 * @return author
	 */

	public Author getAuthorBookData(Author author) {

		String first = author.getAuthorFirst().replace("'", "''");
		String last = author.getAuthorLast().replace("'", "''");

		String sql1 = "SELECT author_first, author_last, henry_book.book_code, title, price, branch_name, on_hand FROM henry_book INNER JOIN henry_wrote on henry_book.book_code = henry_wrote.book_code ";
		String sql2 = "INNER JOIN henry_inventory ON henry_book.book_code = henry_inventory.book_code ";
		String sql3 = "INNER JOIN henry_branch ON henry_inventory.branch_num = henry_branch.branch_num ";
		String sql4 = "INNER JOIN henry_author ON henry_author.author_num = henry_wrote.author_num ";
		String sql5 = "WHERE author_first = '" + first + "' AND author_last = '" + last + "'";
		
		String totalSql = sql1 + sql2 + sql3 + sql4 + sql5;

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(totalSql);

			while (rs.next()) {

				String bookCode = rs.getString("book_code");
				String title = rs.getString("title");
				double price = rs.getDouble("price");
				String branch = rs.getString("branch_name");
				int copiesOfBooks = rs.getInt("on_hand");

				Book book = new Book(bookCode, title, price, branch, copiesOfBooks);
				author.addBook(book);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();

		}

		return author;
	}

	/**
	 * Retrieve list of authors from oracle and store them in an ArrayList
	 * authorList*
	 * 
	 * @return authorList a list with author names
	 */
	public List<Author> getListOfAuthors() {

		String sql = "SELECT author_first, author_last FROM henry_author";
		List<Author> authorList = new ArrayList<>();

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				Author author = new Author(rs.getString("author_first").trim(), rs.getString("author_last").trim());
				authorList.add(author);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return authorList;
	}

	/**
	 * Retrieve list of books from oracle and store them in an ArrayList authorList*
	 * 
	 * @return bookLIst a list with book names
	 */
	public List<Book> getCategoryBookData(String categoryType) {

		String sql1 = "SELECT author_first, author_last, henry_book.book_code, title, price, branch_name, on_hand FROM henry_book INNER JOIN henry_wrote on henry_book.book_code = henry_wrote.book_code ";
		String sql2 = "INNER JOIN henry_inventory ON henry_book.book_code = henry_inventory.book_code ";
		String sql3 = "INNER JOIN henry_branch ON henry_inventory.branch_num = henry_branch.branch_num ";
		String sql4 = "INNER JOIN henry_author ON henry_author.author_num = henry_wrote.author_num" + " WHERE type = '";
		String sql5 = categoryType + "' order by title";

		String totalSql = sql1 + sql2 + sql3 + sql4 + sql5;

		List<Book> bookList = new ArrayList<>();

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(totalSql);

			while (rs.next()) {

				String authorFirst = rs.getString("author_first").trim();
				String authorLast = rs.getString("author_last").trim();

				Author author = new Author(authorFirst, authorLast);

				String bookCode = rs.getString("book_code");
				String title = rs.getString("title");
				double price = rs.getDouble("price");
				String branch = rs.getString("branch_name");
				int copiesOfBooks = rs.getInt("on_hand");

				Book book = new Book(bookCode, title, price, branch, copiesOfBooks);

				author.addBook(book);

				boolean flag = false;
				int bookCount = 0;
				int bookListSize = bookList.size();

				if (bookListSize != 0) {

					while (!(flag || bookListSize <= bookCount)) {

						if (bookList.get(bookCount).equals(book)) {

							bookList.get(bookCount).addBranch(book.getBranch().get(0));
							flag = true;

						} else {

							++bookCount;
						}
					}

					if (bookCount == bookListSize) {

						bookList.add(book);
					}
				} else {

					bookList.add(book);
				}

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return bookList;
	}

	/**
	 * Get a list of categoryType
	 * 
	 * @return categoryType a list storing category types in String
	 **/
	public List<String> getCategoryList() {

		List<String> categoryType = new ArrayList<>();

		String sql = "SELECT DISTINCT type FROM henry_book ORDER BY type";

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				categoryType.add(rs.getString("type"));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return categoryType;
	}

	/**
	 * Get a list of publishers' name
	 * 
	 * @return publisherName a list storing publisherNames in String
	 **/
	public List<String> getPublisherList() {

		List<String> publisherName = new ArrayList<>();

		String sql = "SELECT DISTINCT publisher_name " + "FROM henry_publisher INNER JOIN HENRY_BOOK "
				+ "ON henry_publisher.PUBLISHER_CODE = HENRY_BOOK.PUBLISHER_CODE " + "ORDER BY publisher_name";

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				publisherName.add(rs.getString("publisher_name").trim());
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return publisherName;
	}

	/**
	 * Retrieve list of books from oracle and store them in an ArrayList authorList*
	 * 
	 * @return bookLIst a list with book names
	 */
	public List<Book> getPublisherBookData(String publisher) {

		String sql1 = "SELECT author_first, author_last, henry_book.book_code, title, price, branch_name, on_hand\n";
		String sql2 = "FROM henry_book INNER JOIN henry_wrote \n"
				+ "  ON henry_book.book_code = henry_wrote.book_code \n";
		String sql3 = "  INNER JOIN henry_inventory\n" + "  ON henry_book.book_code = henry_inventory.book_code \n";
		String sql4 = "  INNER JOIN henry_branch \n" + "  ON henry_inventory.branch_num = henry_branch.branch_num \n";
		String sql5 = "  INNER JOIN henry_author \n" + "  ON henry_author.author_num = henry_wrote.author_num\n";
		String sql6 = "  INNER JOIN henry_publisher\n"
				+ "  ON henry_book.PUBLISHER_CODE = henry_publisher.publisher_code\n";
		String sql7 = "  WHERE publisher_name = '" + publisher + "'";

		String totalSql = sql1 + sql2 + sql3 + sql4 + sql5 + sql6 + sql7;

		List<Book> bookList = new ArrayList<>();

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(totalSql);

			while (rs.next()) {

				String authorFirst = rs.getString("author_first").trim();
				String authorLast = rs.getString("author_last").trim();

				Author author = new Author(authorFirst, authorLast);

				String bookCode = rs.getString("book_code");
				String title = rs.getString("title");
				double price = rs.getDouble("price");
				String branch = rs.getString("branch_name");
				int copiesOfBooks = rs.getInt("on_hand");

				Book book = new Book(bookCode, title, price, branch, copiesOfBooks);

				author.addBook(book);

				boolean flag = false;
				int bookCount = 0;
				int bookListSize = bookList.size();
				if (bookListSize != 0) {

					while (!(flag || bookListSize <= bookCount)) {

						if (bookList.get(bookCount).equals(book)) {

							bookList.get(bookCount).addBranch(book.getBranch().get(0));
							flag = true;

						} else {

							++bookCount;
						}
					}

					if (bookCount == bookListSize) {

						bookList.add(book);
					}
				} else {

					bookList.add(book);
				}

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return bookList;
	}

}
	