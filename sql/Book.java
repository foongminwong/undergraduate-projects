
/**
 * Author: Foong Min Wong
 * Program: Book.java
 * 
 * **/

import java.util.ArrayList;
import java.util.List;

public class Book {

	private String bookCode;
	private String title;
	private String publisherCode;
	private String type;
	private Double price;
	private boolean paperback;
	private List<Branch> branch;
	private Author author;

	/** Book Constructor */
	public Book(String bookCode, String title, Double price, String branch, int copiesOfBooks) {
		this.bookCode = bookCode;
		this.title = title;
		this.price = price;
		Branch newBranch = new Branch(branch, copiesOfBooks);
		this.branch = new ArrayList<>();
		this.branch.add(newBranch);

	}

	/**
	 * Set an author
	 * 
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Get an author
	 * 
	 * @return author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * Get Price
	 * 
	 * @return price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * Get a list of bookshop branches
	 * 
	 * @return branch a list of branches
	 */
	public List<Branch> getBranch() {
		return branch;
	}

	/**
	 * Add a new bookShop branch
	 * 
	 */
	public void addBranch(Branch newBranch) {
		branch.add(newBranch);
	}

	// Override Java Class equals method
	public boolean equals(Object object) {
		Book book = (Book) object;
		return (this.bookCode.equals(book.bookCode));
	}

	// Override Java Class toString method
	public String toString() {
		return title;
	}
}
