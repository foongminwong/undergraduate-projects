
/**
 * Author: Foong Min Wong
 * 
 * 
 * Program: SearchByCategoryPanel.java 
 * 
 * **/
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchByCategoryPanel extends JPanel {

	private JComboBox<Book> bookCombo;
	private List<Book> bookList;
	private JTextField authorText;
	private JTextField price;
	// tried JList for branchPanel but it didn't work very well
	private JPanel branchPanel;
	private boolean flag;

	/** SearchByCategoryPanel Constructor **/
	public SearchByCategoryPanel(HenryDAO dao) {

		flag = false;
		authorText = new JTextField();
		price = new JTextField();

		// Set background color
		Color color = new Color(179, 223, 221);
		this.setBackground(color);

		List<String> categoryList = dao.getCategoryList();

		// Create category Combo Box
		JComboBox<String> categoryCombo = new JComboBox<>(new Vector<>(categoryList));

		// addActionListener for CategoryBox
		categoryCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox catBox = (JComboBox) e.getSource();
				String type = (String) catBox.getSelectedItem();
				bookList = dao.getCategoryBookData(type);
				DefaultComboBoxModel<Book> bookComboBoxModel = new DefaultComboBoxModel<Book>(
						new Vector<Book>(bookList));
				bookCombo.setModel(bookComboBoxModel);
				selectedBookInfo(bookList.get(0));
			}
		});
		// add categoryCombo to the panel
		this.add(categoryCombo);

		bookCombo = new JComboBox<>();
		bookCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// grab the combobox and book
				JComboBox bCombo = (JComboBox) e.getSource();
				Book book = (Book) bCombo.getSelectedItem();
				selectedBookInfo(book);

			}
		});

		// add bookCombo to the panel
		this.add(bookCombo);

		bookList = dao.getCategoryBookData(categoryList.get(0));

		authorText.setEditable(false);
		price.setEditable(false);
		this.add(authorText);
		this.add(price);

		DefaultComboBoxModel<Book> bookComboBoxModel = new DefaultComboBoxModel<Book>(new Vector<Book>(bookList));
		bookCombo.setModel(bookComboBoxModel);
		selectedBookInfo(bookList.get(0));

		flag = true;
	}

	/** Set book info once a book is selected **/
	private void selectedBookInfo(Book book) {

		if (flag) {
			this.remove(branchPanel);
		}

		price.setText("$ " + book.getPrice().toString());
		Author a = book.getAuthor();
		authorText.setText(a.getAuthorFirst() + "   " + a.getAuthorLast());

		if (true) {
			int branchSize = book.getBranch().size();
			branchPanel = new JPanel(new GridLayout(branchSize, 3));
			for (Branch branch : book.getBranch()) {
				branchPanel.add(new JLabel(branch.getName()));
				branchPanel.add(new JLabel(branch.getcopiesOfBooks() + ""));
			}
			this.add(branchPanel);
			this.revalidate();
			this.repaint();
		}

	}
}
