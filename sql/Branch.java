
/**
 * Author: Foong Min Wong
 * 
 * Program: Branch.java
 * 
 **/
public class Branch {

	private int copiesOfBooks;
	private String name;

	/**
	 * Branch Constructor
	 **/
	public Branch(String name, int copiesOfBooks) {
		this.name = name;
		this.copiesOfBooks = copiesOfBooks;
	}

	/**
	 * Get copies of the book that branch has on hand.
	 * 
	 * @return copiesOfBooks
	 **/
	public int getcopiesOfBooks() {
		return copiesOfBooks;
	}

	/**
	 * Get name
	 * 
	 * @return name
	 **/
	public String getName() {
		return name;
	}

}
