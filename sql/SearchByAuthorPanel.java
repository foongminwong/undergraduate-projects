
/**
 * Author: Foong Min Wong
 * 
 * 
 * Program: SearchByAuthorPanel.java
 *   
 * **/
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchByAuthorPanel extends JPanel {

	private HenryDAO dao;
	private Author author;
	private JComboBox<Book> bookCombo;
	private JTextField price;
	// tried JList for branchPanel but it didn't work very well
	private JPanel branchPanel;
	private boolean flag;

	/** SearchByAuthorPanle Constructor */
	public SearchByAuthorPanel(HenryDAO dao) {

		// Initialize each variable
		author = null;
		bookCombo = null;
		price = null;
		branchPanel = null;
		flag = false;
		price = new JTextField();

		// Set background color
		Color color = new Color(248, 230, 200);
		setBackground(color);

		// Get a list of authors
		List<Author> authorList = dao.getListOfAuthors();

		// Get the first author
		author = authorList.get(0);

		// Get book data based on the list of authors
		dao.getAuthorBookData(author);

		// Create an authorModel for the combobox that holds the author objects
		DefaultComboBoxModel<Author> authorModel = new DefaultComboBoxModel<>(new Vector<>(authorList));
		JComboBox<Author> authorCombo = new JComboBox<>(authorModel);

		// Add ActionListener for authorCombo
		authorCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JComboBox authorSelect = (JComboBox) e.getSource();

				// have to cast it as Author
				author = (Author) authorSelect.getSelectedItem();
				author = dao.getAuthorBookData(new Author(author.getAuthorFirst(), author.getAuthorLast()));

				// check if an author doesn't have any books
				if (author.getBookList().isEmpty()) {
					JOptionPaneERROR_MESSAGE.main(null);

				} else {
					// Create an bookModel for the combobox that holds the book objects
					DefaultComboBoxModel<Book> bookModel = new DefaultComboBoxModel<Book>(
							new Vector<Book>(author.getBookList()));
					bookCombo.setModel(bookModel);

					Book book = author.getBookList().get(0);
					selectedBookInfo(book);
				}

			}
		});
		// add authorCombo to the panel
		this.add(authorCombo);

		bookCombo = new JComboBox<Book>();

		// Add ActionListener for bookCombo
		bookCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JComboBox bookSelect = (JComboBox) e.getSource();
				// have to cast it to Book
				Book book = (Book) bookSelect.getSelectedItem();
				selectedBookInfo(book);

			}
		});

		// add bookCombo to the panel
		this.add(bookCombo);

		price.setEditable(false);
		this.add(price);

		// Create an bookModel for the combobox that holds the book objects
		DefaultComboBoxModel<Book> bookModel = new DefaultComboBoxModel<Book>(new Vector<Book>(author.getBookList()));
		bookCombo.setModel(bookModel);

		// get the first book
		Book book = author.getBookList().get(0);
		selectedBookInfo(book);

		flag = true;

	}

	/** Set book info once a book is selected **/
	private void selectedBookInfo(Book book) {
		price.setText("$ " + book.getPrice());

		if (flag) {
			this.remove(branchPanel);
		}

		if (true) {
			int branchSize = book.getBranch().size();
			branchPanel = new JPanel(new GridLayout(branchSize, 3));
			for (Branch branch : book.getBranch()) {
				branchPanel.add(new JLabel(branch.getName()));
				branchPanel.add(new JLabel(branch.getcopiesOfBooks() + ""));
			}
			this.add(branchPanel);
			this.revalidate();
			this.repaint();
		}

	}

}
