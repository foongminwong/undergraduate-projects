/*
 * Created by Foong Min Wong
 * Lab 3 Pointers and Heap Memory
*/

#include<stdio.h>
#include<stdlib.h>

// structure
typedef struct freq{
	int number;
	int frequency;
}Histogram;

// prototype
int* readScores(int* size);
void displayScores(int* scoreTest, int* size);
int calcHistogram(int* scoreTest, int size, Histogram* histograms);
void displayHistogram(int arrayStrucSize, Histogram* histograms);
void sortHistogram(int arrayStrucSize, Histogram* histograms);

int main(){

	int size = 0;
	
	Histogram histograms[100];
	int* scoreTest  = readScores(&size);
	
	displayScores(scoreTest,&size);

	int arrayStrucSize  = calcHistogram(scoreTest, size, histograms);
	
	displayHistogram(arrayStrucSize, histograms);
	
	sortHistogram(arrayStrucSize, histograms);
	
	//free the elements inside the array
	//int k;
	//for(k = 0; k < size; k++){
		//free(&scoreTest[k]);
	//}
	free(scoreTest);

}

// read scrores
int* readScores(int* size){
	int x; 
	int i = 0;
	
	int* scores = (int*) malloc(50 * sizeof(int)); // arrays created on the heap
	
	while(scanf("%d",&x)!=EOF){
		scores[i] = x; // *(score+i) = x; 
		(*size)++; // increment size
		i++;
	}

	return scores;
}

// display scores
void displayScores(int* scoreTest, int* size){

	printf("--- Display scores ---\n");
	int counter = 0;
	while(counter < *size){
		printf("score %d: %d\n",counter,scoreTest[counter]);
		counter++;
	}
	printf("\n");
}

// calculate histogram
int calcHistogram(int* scoreTest, int size, Histogram* histograms){	
	// arrays created on the heap
	Histogram* histograms2 = (Histogram*) malloc(50 * (sizeof(histograms)));
	
	int i,j;
	int arrayStrucSize = 0; 
	
	for(i = 0; i < size; i++){
		int value = scoreTest[i]; //int value = *(score+i);
		
		j = 0;
		while((j< arrayStrucSize) && (histograms2[j].number != value)){
			j++;
		}
			 if(j < arrayStrucSize ){
				
				 (histograms2[j].frequency)++;
			 }
			 else{
				 histograms2[j].number = value;
				 histograms2[j].frequency = 1; //occurs one time 
				 (arrayStrucSize)++; // increment 
				
			 }
	}
	
	int k = 0;
	while(k < arrayStrucSize){
		histograms[k].number = histograms2[k].number;
		histograms[k].frequency = histograms2[k].frequency;
		k++;
	}
	

	free(histograms2);
	return arrayStrucSize;
}

// display histogram
void displayHistogram(int arrayStrucSize, Histogram* histograms){

	printf("--- Display Histogram ---\n");
	int counter = 0;
	while(counter < arrayStrucSize){
		printf("value %d: freq %d\n",((histograms[counter])).number,((histograms[counter])).frequency);
		counter++;
	}
	printf("\n");
}

// sort histogram
void sortHistogram(int arrayStrucSize, Histogram* histograms){

	printf("--- Display Sorted Histogram ---\n");
	int i, j;
	j = 0;
	int tempFreq;
	int tempNum;
	
	for(i = 0; i < arrayStrucSize; i++){
		
		for(j = i + 1;j < arrayStrucSize; j++ ){
			
			if(((histograms[i]).frequency) < ((histograms[j]).frequency)){
				tempFreq = (histograms[i].frequency);
				(histograms[i].frequency) = (histograms[j].frequency);
				(histograms[j].frequency) = tempFreq;
				
				tempNum = (histograms[i].number);
				(histograms[i].number) = (histograms[j].number);
				(histograms[j].number) = tempNum;
				
				
			}
			
		}
	
	}
	
	int counter = 0;
	while(counter < arrayStrucSize){
		printf("value %d: freq %d\n",((histograms[counter])).number,((histograms[counter])).frequency);
		counter++;
	}
	
	printf("\n");
}

