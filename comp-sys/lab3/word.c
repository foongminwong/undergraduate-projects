#include<stdio.h>
#include<stdlib.h>
#include<string.h> // strlen, strcmp, strcpy

//structure String
typedef struct str {
	char* words;
	int frequency;
 }String;

  
//prototype
char** readStrings(int* size);
void displayStrings1(char** stringsTest, int* size);
int calcString(char** stringsTest, int size, String** stringStruc);
void displayStrings2(int arrayStrucSize, String* stringStruc);
void sortString(int arrayStrucSize, String* stringStruc);
 
int main(){

	int size = 0; //remember to initialize to zero
	
	String* stringStruc;// pointer to the struc

	// printf("readStrings\n");
	char** stringsTest  = readStrings(&size); // pointer to string
	printf("\n");

	printf("--- Display words from file ---\n");
	displayStrings1(stringsTest,&size);
	printf("\n");
	
	//printf("calcString\n");
	int arrayStrucSize  = calcString(stringsTest, size, &stringStruc);
	printf("\n");
	
	//printf("displayStrings2\n");
	displayStrings2(arrayStrucSize, stringStruc);
	
	//printf("sortString\n");
	sortString(arrayStrucSize, stringStruc);
	
	//use loop to free the elements inside the array
	int k;
	// not k < size is k<100 you need to free the other empty space
	for(k = 0; k < 100; k++){
		free(stringsTest[k]);
	}
	
	free(stringsTest);
	free(stringStruc);
}

char** readStrings(int* size){
	//printf("readStrings\n");
	(*size) = 0;
	char* x;
	int i = 0;
	char** strings = (char**) malloc(100 * sizeof(char*)); // arrays created on the heap
	//printf("beforefor\n");
	for(i = 0; i < 100; i++){
		x = (char*) malloc(100 * sizeof(char));
		strings[*size] = x;
		(*size)++;
	}
	
	(*size) = 0;
	
	//read files
	//printf("beforewhile\n");
	int y = scanf("%s",strings[*size]);
	//printf("afterscan\n");
	//printf("<%s>\n",strings[*size]);
	while(y !=EOF){
		//printf("inwhile\n");
		//printf("%s\n",strings[*size]);
		(*size)++;
		y = scanf("%s",strings[*size]); // the 2nd stuff
	}
	return strings;
}

void displayStrings1(char** stringsTest, int* size){
	int counter = 0;
	while(counter < *size){

		//printf("%s\n",stringsTest+counter);

		printf("%s ",stringsTest[counter]); 
		//printf("%s\n",stringsTest); << seg fault
		//printf("%s\n",stringsTest+counter); << seg fault
		counter++;
	}
}

int calcString(char** stringsTest, int size, String** stringStruc){

	//size = 14;
	//Histogram* stringStruc2 = (Histogram*) malloc(50 * (sizeof(int))); // arrays created on the heap
	String* stringStruc2 = (String*) malloc(50 * (sizeof(String)));
	
	int i,j;
	int arrayStrucSize = 0; 
	
	for(i = 0; i < size; i++){
		char* word = stringsTest[i]; //int word = *(score+i);
		//printf("%s\n",word);
		j = 0;
		while((j< arrayStrucSize) && (strcmp(stringStruc2[j].words,word)) != 0){
			j++;
		}
			 if(j < arrayStrucSize ){
				
				 (stringStruc2[j].frequency)++;
			 }
			 else{
				 stringStruc2[j].words = word;
				 stringStruc2[j].frequency = 1; //occurs one time 
				 (arrayStrucSize)++; // increment 
				
			 }
	}
	
	//int k = 0;
	//while(k < arrayStrucSize){
		//stringStruc[k].words = stringStruc2[k].words;
		//stringStruc[k].frequency = stringStruc2[k].frequency;
		//k++;
	//}
	*stringStruc = stringStruc2;
	//free(stringStruc2);
	return arrayStrucSize;
}

void displayStrings2(int arrayStrucSize, String* stringStruc){
	int counter = 0;
	printf("--- Display Histogram ---\n");
	
	while(counter < arrayStrucSize){
		printf("word %s: freq %d\n",(stringStruc[counter]).words,(stringStruc[counter]).frequency);
		counter++;
	}
	printf("\n");
}

void sortString(int arrayStrucSize, String* stringStruc){

	printf("--- Display Sorted Histogram ---\n");

	int i, j;
	j = 0;
	int tempFreq;
	char* tempWord;
	
	for(i = 0; i < arrayStrucSize; i++){
		
		for(j = i + 1;j < arrayStrucSize; j++ ){
			
			if(((stringStruc[i]).frequency) < ((stringStruc[j]).frequency)){
				tempFreq = (stringStruc[i].frequency);
				(stringStruc[i].frequency) = (stringStruc[j].frequency);
				(stringStruc[j].frequency) = tempFreq;
				
				tempWord = (stringStruc[i].words);
				(stringStruc[i].words) = (stringStruc[j].words);
				(stringStruc[j].words) = tempWord;
				
			}
			
		}
	
	}
	
	int counter = 0;
	while(counter < arrayStrucSize){
		printf("word %s: freq %d\n",((stringStruc[counter])).words,((stringStruc[counter])).frequency);
		counter++;
	}
	
	printf("\n");
	
}
