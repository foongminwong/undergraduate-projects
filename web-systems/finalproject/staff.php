<!DOCTYPE html>
<html>

<head>
  <title>UWEC CS | Faculty + Staff</title>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="css/scrolling-nav.css" rel="stylesheet">


</head>
<style>
.hovereffect {
  width: 50%;
  height: 50%;
  float: left;
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: default;
  background: #ffffff;
  font-size: 20px;


}

.hovereffect .overlay {
  width: 50%;
  height: 50%;
  position: absolute;
  overflow: hidden;
  top: 0;
  left: 0;
  padding: 50px 20px;
}

.hovereffect img {
  display: block;
  position: relative;
  max-width: none;
  width: calc(100% + 20px);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(-10px,0,0);
  transform: translate3d(-10px,0,0);
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
}

.hovereffect:hover img {
  opacity: 0.4;
  filter: alpha(opacity=40);
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}

.hovereffect h2 {
  text-transform: uppercase;
  color: #fff;
  text-align: center;
  position: relative;
  font-size: 17px;
  overflow: hidden;
  padding: 0.5em 0;
  background-color: transparent;
}

.hovereffect h2:after {
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 2px;
  background: #fff;
  content: '';
  -webkit-transition: -webkit-transform 0.35s;
  transition: transform 0.35s;
  -webkit-transform: translate3d(-50%,0,0);
  transform: translate3d(-50%,0,0);
}

.hovereffect:hover h2:after {
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}

.hovereffect a, .hovereffect p {
  color: #FFF;
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(100%,0,0);
  transform: translate3d(100%,0,0);
}

.hovereffect:hover a, .hovereffect:hover p {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}
</style>
<body id="page-top" style ="font-family: 'Roboto Condensed', sans-serif;">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
      <!-- <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="images/uwec-horizontal-wordmark_UWEC-horizontal-wordmark-white_rbg.png" alt="Power-of-AND" height="55%" width="55%"></a> -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">

          <li class="nav-item">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="course.php">Courses</a>
          </li>

          <li class="nav-item active">
            <a class="nav-link js-scroll-trigger" href="#facultystaff">Faculty+Staff</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="login.php">Login</a>
          </li>

        </ul>
      </div>
    </div>
  </nav>

  <header class="bg-primary text-white">
    <div class="container text-center">
      <h1><font color="#edac1a">UWEC CS</font> Faculty + Staff</h1>
      <p class="lead">We have a team dedicated to helping you succeed and get started on all of your computer science goals. Whether it be landing an internship, securing your job, getting into a specific class, we're ready to help you. We have a wide knowledge base amongst the group and we're excited to share what we know with you! Get to know us. </div>

  <section id="facultystaff" >
    <div class="container">
      <div class="row">
<div class="hovereffect">
        <img src="cs_faculty/here/JT_Collage.png" alt="Jack Tan" height="50%" width="50%">
 <div class="overlay">
				<p>
					<a href="https://cs.uwec.edu/~tan/" target="_blank">Jack Tan</a>
				</p>
</div>
</div>


<div class="hovereffect">
        <img src="cs_faculty/here/HA_Collage.png" alt="Heather Amthauer" height="50%" width="50%">
 <div class="overlay">
        <p>
					<a href="" target="_blank">Heather Amthauer</a>
				</p>
</div>
</div>

<div class="hovereffect">
        <img src="cs_faculty/here/RH_Collage.png" alt="Ryan Hardt" height="50%" width="50%">
        <div class="overlay">
               <p>
       					<a href="https://cs.uwec.edu/~hardtr/" target="_blank">Ryan Hardt</a>
       				</p>
       </div>
       </div>

<div class="hovereffect">
        <img src="cs_faculty/here/CJ_Collage.png" alt="Chris Johnson" height="50%" width="50%">
        <div class="overlay">
               <p>
                 <a href="https://twodee.org/blog/" target="_blank">Chris Johnson</a>
               </p>
       </div>
       </div>

<div class="hovereffect">
        <img src="cs_faculty/here/DS_Collage.png" alt="Daniel Stevenson" height="50%" width="50%">
        <div class="overlay">
               <p>
                 <a href="https://cs.uwec.edu/~stevende/" target="_blank">Daniel Stevenson</a>
               </p>
       </div>
       </div>

<div class="hovereffect">
        <img src="cs_faculty/here/AC_Collage.png" alt="Alexander Cobian" height="50%" width="50%">
        <div class="overlay">
               <p>
                 <a href="" target="_blank">Alexander Cobian</a>
               </p>
       </div>
       </div>

<div class="hovereffect">
        <img src="cs_faculty/here/MC_Collage.png" alt="Michelle Cicha" height="50%" width="50%">
        <div class="overlay">
               <p>
                 <a href="" target="_blank">Michelle Cicha</a>
               </p>
       </div>
       </div>

<div class="hovereffect">
        <img src="cs_faculty/here/Pet_Collage.png" alt="Penguin" height="50%" width="50%">
        <div class="overlay">
               <p>
                 <a href="" target="_blank">Penguin</a>
               </p>
       </div>
       </div>


      </div>
    </div>
  </section>
  </header>









  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &#169; 2018 University of Wisconsin - Eau Claire Computer Science Department</p>
      <br>
      <a href="#page-top" class="js-scroll-trigger">  <p class="m-0 text-center"><img src="images/Power-of-AND_horz_wht_RGB_web.png" alt="Power-of-AND" height="20%" width="20%"></p></a>

    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery.min.js"></script>
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

  <!-- Plugin JavaScript -->
  <script src="jquery/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>

</body>

</html>
