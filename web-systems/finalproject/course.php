<?php
include_once 'db_connection.php';
$con = new Connection();
$conn = $con->openConnection();

?>

<!DOCTYPE html>
<html>

<head>
  <title>UWEC CS | Courses</title>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="css/scrolling-nav.css" rel="stylesheet">
  <!-- stylesheets -->
      <link rel="stylesheet" href="Treant.css" type="text/css"/>
</head>
<style>

</style>
<body id="page-top" style ="font-family: 'Roboto Condensed', sans-serif;">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
      <!-- <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="images/uwec-horizontal-wordmark_UWEC-horizontal-wordmark-white_rbg.png" alt="Power-of-AND" height="55%" width="55%"></a> -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item active">
            <a class="nav-link js-scroll-trigger" href="#course">Courses</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="staff.php">Faculty+Staff</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="login.php">Login</a>
          </li>

        </ul>
      </div>
    </div>
  </nav>

  <header class="bg-primary text-white">
    <div class="container text-center">
      <h1><font color="#edac1a">UWEC CS</font> Courses</h1>
      <p class="lead">Computer Science and Software Engineering are disciplines that combine concepts from mathematics, science, and engineering into a coherent and disciplined study of the software and hardware required for computation. The programs offered by the UW-Eau Claire Department of Computer Science provide the fundamental education necessary to prepare students for positions in industry or to pursue graduate study. The curriculum is organized so that graduates will be able to meet immediate demands for solving computational problems or designing state-of-the-art computer systems, yet also have an understanding of the basic principles and concepts in computer science needed to avoid technological obsolescence in a rapidly changing environment. This program is intended to produce computer science professionals, not merely technicians with some training in computer software and hardware. Success requires a strong aptitude in mathematics
       </div>

  <section id="course" >
    <div class="container">
        <div class="content">
          <div class="container-fluid">
            <?php
            $statement = $conn->prepare("SELECT * FROM wongf3284_courses");
            $statement->execute();
            $result = $statement->fetchAll();
            $item = 1;
            ?>
            <?php

      echo "<div class=\"row\">\n";
              //echo "<div class=\"row\">\n";
              foreach ($result as $row) {

       echo "<div class=\"col-3\">\n";


        echo "<h3><p class=\"text-center\">".$row['courseid']."</p></h3>";
          echo "<h5><p class=\"text-center\">".$row['coursename']."</p></h5><br><br>";
      $item++;
       echo "<div class=\"col-3\">\n";

        echo "</div>\n";
        echo "</div>\n";


              }
                echo "</div>\n";
              ?>
          </div>
        </div>
</div>
  </section>
  </header>









  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &#169; 2018 University of Wisconsin - Eau Claire Computer Science Department</p>
      <br>
      <a href="#page-top" class="js-scroll-trigger">  <p class="m-0 text-center"><img src="images/Power-of-AND_horz_wht_RGB_web.png" alt="Power-of-AND" height="20%" width="20%"></p></a>

    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery.min.js"></script>
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

  <!-- Plugin JavaScript -->
  <script src="jquery/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>

</body>

</html>
