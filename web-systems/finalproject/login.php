<!DOCTYPE html>
<html>

<head>
  <title>UWEC CS | Login</title>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="css/scrolling-nav.css" rel="stylesheet">


</head>

<style>
body {text-align:center;}

.error{
  color:red;
}

.create{
  color:green;
}

.p{
  font-style: italic;
}

</style>
</head>

<body id="page-top" style ="font-family: 'Roboto Condensed', sans-serif;">
  <header class="bg-primary text-white">
    <div class="container text-center">
      <h1><font color="#edac1a">UWEC CS</font> Login</h1>
      <br>
  <p class="lead" font-style="italic">
  <?php

  $quoteList = array(
    "\"Work Hardt. Play Hardt.\"",
      "\"The more I C, the less I see.\"",
      "\"Life would be so much easier if we only had the source code.\"",
      "\"Campus alert: No class today!\"",
          "\"SACM is having a club meeting this Wednesday!\"",
            "\"Free pizza in MCIS Lab!\"",
              "\"No office hour today. (Cobian, 2018)\"",
                            "\"Life is really simple, but we insist on making it complicated. (Confucius)\""

  );
echo $quoteList[mt_rand(0, count($quoteList)-1)];
  ?>
  </p>
<br>
  <form action="login.php" method="post">
	Username: <input type="text" name="user" id="user" value="<?php if(isset($_POST['user'])) { echo $_POST['user']; } ?>"><br><br>
	Password: <input type="password" name="pass" id="pass"><br><br>
	<input type="submit" name="login" id="login" value="Login" /><br><br>
	<!-- <input type="submit" name="register" id="register" value="Register" /> -->
	<br><br>
</form>

<?php
session_start();
//Connections
include_once 'db_connection.php';
$con = new Connection();

//check whether a variable is set or not
if(isset($_POST['user']) && isset($_POST['pass'])){
  $user= $_POST['user'];
  $pass = $_POST['pass'];


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // login
    if (isset($_POST['login'])) {

      // check empty
      if(empty($user) || empty($pass)) {
      echo 'All field are required';
      }

      // check username/password
      else{
        $conn = $con->openConnection();
        $statement = $conn->prepare("SELECT * FROM wongf3284_users WHERE username= ? AND password = ?");
        $statement->bindParam(1,$user);
        $statement->bindParam(2,$pass);
        $statement->execute();
        $result = $statement->fetchAll();

        // username and password correct
        if($statement->rowCount() == 1){
            $_SESSION['user'] = $user;
            $_SESSION['login'] = true;
        header("Location: work.php");
        }

        else{
          $statement = $conn->prepare("SELECT username FROM wongf3284_users WHERE username= ?");
          $statement->bindParam(1,$user);
          $statement->execute();
          $result = $statement->fetchAll();

          if($result == null){
            echo "<span class=\"error\">No user $user exists.</span>";
          }

          else{
            echo "<span class=\"error\">Incorrect password.</span>";
           }
      }
    }
}

    //register
    else {
      if(empty($user) || empty($pass)) {
      echo 'All field are required';
    }

    //successful registeration
    else{

      $conn = $con->openConnection();
      $statement = $conn->prepare("SELECT * FROM wongf3284_users WHERE username= ?");
      $statement->bindParam(1,$user);
      $statement->execute();
      $result = $statement->fetchAll();

      if(strlen($user) < 4 || strlen($user) > 31){
      echo "<span class=\"error\">Invalid
username: Must be between 4 and 31 characters long.</span>";

}else if(!ctype_alnum($user)){
echo "<span class=\"error\">Invalid username: Must contain only numbers and letters.</span>";
}

else if(strlen($pass) < 8 || strlen($pass) > 31){
echo "<span class=\"error\">Invalid password: Must be between 8 and 31 characters long.</span>";
}

// check password at least one lowercase, uppercase and numeric character
else if(!preg_match("/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/", $pass)){
  echo "<span class=\"error\">Invalid password: Must contain lowercase
letters, uppercase letters, and numbers.</span>";

}else if($statement->rowCount() == 1){
  echo "<span class=\"error\">User $user already exists.</span>";

}else{
  $sessionclick = 0;
  $statement = $conn->prepare("INSERT INTO wongf3284_users VALUES (?,?)");
  $statement->bindParam(1,$user);
  $statement->bindParam(2,$pass);
  $statement->execute();

  echo "<span class=\"create\">Account $user created!</span>";

}

}
}
}

}

?>
  </header>

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &#169; 2018 University of Wisconsin - Eau Claire Computer Science Department</p>
      <br>
      <p class="m-0 text-center"><a href="index.php"><img src="images/Power-of-AND_horz_wht_RGB_web.png" alt="Power-of-AND" height="20%" width="20%"></a></p>

    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery.min.js"></script>
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

  <!-- Plugin JavaScript -->
  <script src="jquery/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>

</body>

</html>
