Name: Foong Min Wong
Description: This project is a simple reconstruction of UWEC Computer Science Department website.
PHP Files:
1. index.php: This is the homepage of UWEC CS Department website.
2. course.php: This is a page listing courses at UWEC CS Department.
3. staff.php:  This is a page listing the staff at UWEC CS Department.
4. login.php: This is a login page for existing users in database (wongf3284_users.sql). It will generate random quotes whenever the user refreshes the page.
5. work.php: This is a directed page from login page once a user logins successfully. It reminds the staff/student to grade/homework.
6. db_connection.php: This connects to the database "wongf3284_finalproject.sql"

SQL Files:
1. wongf3284_finalproject.sql
-> wongf3284_courses.sql (table)
-> wongf3284_users.sql (table)

CSS Files:
1. bootstrap.min.css: CSS for almost all pages to ensure consistent design in each page
2. scrolling-nav.css: CSS for navigation menu
3. style.css: CSS for the clock
4. style2.css: CSS for social media icons

Javascript/JQuery files:
1. index.js: Contains the function for the clock in work page (to show real time to login user)
2. scrolling-nav.js: Contains function
3. jquery.easing.min.js
4. jquery.min.js

Folders:
1. images/
2. cs_faculty/

References:
1. https://www.w3schools.com/
2. https://getbootstrap.com/
3. http://freefrontend.com/
4. https://codepen.io/
