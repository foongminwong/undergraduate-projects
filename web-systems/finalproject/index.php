<!DOCTYPE html>
<html>

<head>
  <title>UWEC CS | Home</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="css/scrolling-nav.css" rel="stylesheet">

  <link rel="stylesheet" href="css/style2.css">

</head>

<style>

  .thumb {
    padding-bottom: 60%;
    background-size: cover;
    background-position: center center;
  }
}

.item-1 {
  @media (min-width: 60em) {
    grid-column: 1 / span 2;

  }
}

.container1 {
  width: 500px;
  height: 500px;
  margin: 0 auto;
  position: relative;
  -webkit-perspective: 1000;
	-moz-perspective: 1000;
	perspective: 1000;
  -moz-transform: perspective(1400px);
	-ms-transform: perspective(1400px);
	-webkit-transform-style: preserve-3d;
  -moz-transform-style: preserve-3d;
  transform-style: preserve-3d;
  -webkit-perspective-origin: right;
  -moz-perspective-origin: right;
  perspective-origin: right;
}
.card {
  width: 500px;
  height: 500px;
  box-shadow: 0 27px 55px 0 rgba(0, 0, 0, .7), 0 17px 17px 0 rgba(0, 0, 0, .5);
  position: relative;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -ms-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transform-origin: 100% 0%;
  -moz-transform-origin: 100% 0%;
  -ms-transform-origin: 100% 0%;
  transform-origin: 100% 0%;
  -webkit-transform-style: preserve-3d;
  -moz-transform-style: preserve-3d;
  transform-style: preserve-3d;
  transition: .8s ease-in-out;
}
.logo {
   background-image:url('images/p.png');
   width: 500px;
   height: 500px;
}
.logo:before {
  /* content: ""; */
  position: absolute;
  width: 500px;
  height: 500px;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.logo:after {
  width: 500px;
  height: 500px;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.logo span {
  display: block;
  width: 500px;
  height: 500px;
  border-radius: 0 50% 50% 0;
}
.logo span:before {
  content: "";
  width: 500px;
  height: 500px;
  border-radius: 50%;
  position: absolute;
  z-index: 2;
}
.front, .back {
  position: absolute;
  width: 500px;
  height: 500px;
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility: hidden;
  backface-visibility: hidden;
}
.front {
  display:-webkit-flex;
  display: flex;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-align-items: center;
  align-items: center;
  z-index: 2;
  -webkit-transform: rotateY(0deg);
  -moz-transform: rotateY(0deg);
  -ms-transform: rotateY(0deg);
  transform: rotateY(0deg);
}
.back {
  -webkit-transform: rotateY(-180deg);
  -moz-transform: rotateY(-180deg);
  -ms-transform: rotateY(-180deg);
  transform: rotateY(-180deg);
}
.container1:hover .card {
  -webkit-transform: rotateY(180deg) translateX(100%);
  -moz-transform: rotateY(180deg) translateX(100%);
  -ms-transform: rotateY(180deg) translateX(100%);
  transform: rotateY(180deg) translateX(100%);
  cursor: pointer;
}


</style>

<body id="page-top" style ="font-family: 'Roboto Condensed', sans-serif;">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
      <!-- <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="images/uwec-horizontal-wordmark_UWEC-horizontal-wordmark-white_rbg.png" alt="Power-of-AND" height="55%" width="55%" href="index.php"></a> -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">

          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#career">Career</a>
          </li>


                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="#review">Reviews</a>
                    </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#social">Social</a>
          </li>


          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="course.php">Courses</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="staff.php">Faculty+Staff</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="login.php">Login</a>
          </li>

        </ul>
      </div>
    </div>
  </nav>

  <header class="bg-primary text-white">
    <div class="container text-center">
      <h1><font color="#edac1a">UWEC</font> Computer Science</h1>
      <!-- <font color="#2b3e85">Computer Science</font> -->
      <p class="lead">From your first line of code to programming for hours on end, our computer science program is equipped to prepare you for future. With excellent and dedicated professors working alongside our hardworking and high-achieving students, we're sure you'll find your place right here in computer science. Our department has the motherboard of courses and instructors to launch your computer science career.</dp>
  </header>

  <section id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <div class="bgimg">
          <h2>About</h2>
          <p class="lead">The computer science department focuses on excellence in educating graduates with the fundamentals of computer science and skills to solve complex technological problems and lead the computing industry in the broad aspects of computer science. Students
            are exposed to multidisciplinary domains through student/faculty research projects that provide creative leading-edge solutions for both industrial and societal needs. Students are also able to communicate their ideas clearly and concisely and
            are also fully committed to the ethical practices in their field of expertise.</p>
        </div>
      </div>
    </div>
  </section>




  <section id = "contact" class="bg-light">
    <div class="container1 text-center">
      <div class="card">
        <div class="front">
        <img class="d-block w-100" src="images/and.png" height=500px alt="Third slide">
        </div>
        <div class="back">
              <img class="d-block w-100" src="images/andand.png" height="500px alt="Third slide">
            <!-- <h1><font color="#edac1a">UWEC</font> Computer Science</h1> -->

          <!-- <ul> -->
            <!-- <li>715-836-2526</li> -->
            <!-- <li>csinfo@uwec.edu</li> -->

            <!-- </ul> --> -->
        </div>
        </div>
      </div>
</div>
  </section>

  <section id="career">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <div class="bgimg">
          <h2>Career</h2>
          <p class="lead">Software engineers and programmers are needed in virtually every sector of the economy, making this career path one of the fastest growing job markets in the United States. Our rigorous, detail-oriented, and challenging curriculum prepares our students for life beyond the classroom. You'll get the necessary experience to be highly competitive in the workforce.

Our graduates go on to do incredible things as software engineers, developers, analysts, and programmers. Some even go on to further their education at some of the nation's top universities in computer science.</p>
        </div>
      </div>
    </div>
  </section>

  <section id="review" class="bg-light">
    <div class="container" height="50%" width="50%">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" height="50%" width="50%">
  <ol class="carousel-indicators" height="50%" width="50%">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="images/bill1.png" height="50%" width="50%" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/mark1.png" height="50%" width="50%" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/elon1.png" height="50%" width="50%" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  </div>
    </div>
  </section>


<section id ="social" >
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
            <h2>Connect With Us</h2>
      <p class="lead">It's true what they say, "once a Blugold, always a Blugold." It's very important to us that our graduates stay connected and always feel that they are part of the Blugold family. </p>
</div>
</div>

<div id="circ">
  <ul>
    <li class="animated zoomIn">
      <div class="back"></div>
      <div class="icon">
        <a href="https://www.facebook.com/UWEC-Computer-Science-Department-402734582274/"><i class="fa fa-facebook"></i></a>
      </div>
    </li>
    <li class="animated zoomIn delayone">
      <div class="back"></div>
      <div class="icon">
        <a href="https://twitter.com/uwecsacm"><i class="fa fa-twitter"></i></a>
      </div>
    </li>
    <li class="animated zoomIn delaythree">
      <div class="back"></div>
      <div class="icon">
        <a href="https://www.youtube.com/user/UWEauClaire"><i class="fa fa-youtube"></i></a>
      </div>
    </li>
  </ul>
</div>
</div>
</section>



  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &#169; 2018 University of Wisconsin - Eau Claire Computer Science Department</p>
      <br>
      <a href="#page-top" class="js-scroll-trigger"><p class="m-0 text-center"><img src="images/Power-of-AND_horz_wht_RGB_web.png" alt="Power-of-AND" height="20%" width="20%"></p></a>

        <a class="navbar-brand js-scroll-trigger" href="https://maplestory.design/" target="_blank">CreateAvatar</a>

    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery.min.js"></script>
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

  <!-- Plugin JavaScript -->
  <script src="jquery/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>

</html>
