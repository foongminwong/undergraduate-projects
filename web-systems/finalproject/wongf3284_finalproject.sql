-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2018 at 09:08 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wongf3284_finalproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `wongf3284_courses`
--

CREATE TABLE `wongf3284_courses` (
  `id` int(11) NOT NULL,
  `courseid` varchar(255) DEFAULT NULL,
  `coursename` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wongf3284_courses`
--

INSERT INTO `wongf3284_courses` (`id`, `courseid`, `coursename`) VALUES
(1, 'CS145', 'Programming for New Programmers'),
(2, 'CS148', 'Programming for Experienced Programmers '),
(3, 'CS163', 'Introduction to Programming in C++'),
(4, 'CS245', 'Advanced Programming and Data Structures'),
(5, 'CS252', 'Computer Systems'),
(6, 'CS260', 'Database Systems'),
(7, 'CS268', 'Web Systems'),
(8, 'CS330', 'Programming Languages'),
(9, 'CS335', 'Algorithms'),
(10, 'CS352', 'Computer Architecture'),
(11, 'CS355', 'Software Engineering I'),
(12, 'CS396', 'Junior Seminar'),
(13, 'CS452', 'Operating Systems'),
(14, 'CS462', 'Computer Netwroks'),
(15, 'CS485', 'Software Engineering II');

-- --------------------------------------------------------

--
-- Table structure for table `wongf3284_users`
--

CREATE TABLE `wongf3284_users` (
  `username` varchar(31) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `wongf3284_users`
--

INSERT INTO `wongf3284_users` (`username`, `password`) VALUES
('amthauha', '123'),
('hardtr', '123'),
('johnch', '123'),
('stevende', '123'),
('wongf3284', '123'),
('uwec0007', '123'),
('cobianag', '123'),
('tanjs', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wongf3284_courses`
--
ALTER TABLE `wongf3284_courses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wongf3284_courses`
--
ALTER TABLE `wongf3284_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
