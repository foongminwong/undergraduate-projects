<!-- UPDATE users SET clicks = 5 WHERE username = "oppagangnam123"; -->
<!-- 1234OPPkls -->
<?php
//Connections
session_start();
include_once 'db_connection.php';
$user =   $_SESSION['user'];

$con = new Connection();
$conn = $con->openConnection();
$statement = $conn->prepare("SELECT clicks FROM users WHERE username= ?");
$statement->bindParam(1,$user);
$statement->execute();
$result = $statement->fetchAll();
$clicks = $result[0]['clicks'];

if(!isset($_SESSION['counter'])) {
    $_SESSION['counter'] = 0;
}

// no active session
if(!isset($_SESSION['user'])){
  header("Location: login.php");
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // logout
    if (isset($_POST['logout'])) {

$_SESSION['from2'] = true;
      unset($_SESSION['logout']);
  header("Location: login.php");

exit();

    }
// work
else if(isset($_POST['work'])){
  $statement = $conn->prepare("UPDATE users SET clicks = clicks + 1 WHERE username= ?");
  $statement->bindParam(1,$user);
  $statement->execute();

  $statement = $conn->prepare("SELECT clicks FROM users WHERE username= ?");
  $statement->bindParam(1,$user);
  $statement->execute();
  $result = $statement->fetchAll();
  $clicks = $result[0]['clicks'];

  ++$_SESSION['counter'];
}

}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Work Website | <?php echo $user?> </title>
  <style>
  body {text-align:center;}
  .error{
    color:red;
  }
  .create{
    color:green;
  }
  </style>
</head>
<body>

<form action="work.php" method="post">
  <h1> Work harder, <?php echo $user?></h1>
  <p> You have done <?php echo $_SESSION['counter']?> units of work this session.</p>
  <p> You have done <?php echo $clicks?> units of work across all sessions.</p>
  <br>
  <input type="submit" name="work" id="work" value="Work" /><br><br>
  <input type="submit" name="logout" id="logout" value="Logout" />
  <br><br>
</form>

</body>
</html>






<!-- echo "<span class=\"create\">You have been logged out.</span>"; -->
