<!DOCTYPE html>
<!-- html, css -->
<html><head>
  <title>Work Website</title>
  <style>
  body {text-align:center;}

  .error{
    color:red;
  }

  .create{
    color:green;
  }
  </style>
</head>

<!--  body starts here-->
<body>
  <form action="login.php" method="post">
	<h1> Welcome to the Work Website</h1>
	Username: <input type="text" name="user" id="user" value="<?php if(isset($_POST['user'])) { echo $_POST['user']; } ?>"><br><br>
	Password: <input type="password" name="pass" id="pass"><br><br>
	<input type="submit" name="login" id="login" value="Login" /><br><br>
	<input type="submit" name="register" id="register" value="Register" />
	<br><br>
</form>

<!-- PHP starts here -->
<?php
session_start();
//Connections
include_once 'db_connection.php';
$con = new Connection();

//check whether a variable is set or not
if(isset($_POST['user']) && isset($_POST['pass'])){
  $user= $_POST['user'];
  $pass = $_POST['pass'];


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // login
    if (isset($_POST['login'])) {

      // check empty
      if(empty($user) || empty($pass)) {
      echo 'All field are required';
      }

      // check username/password
      else{
        $conn = $con->openConnection();
        $statement = $conn->prepare("SELECT * FROM users WHERE username= ? AND password = ?");
        $statement->bindParam(1,$user);
        $statement->bindParam(2,$pass);
        $statement->execute();
        $result = $statement->fetchAll();

        // username and password correct
        if($statement->rowCount() == 1){
            $_SESSION['user'] = $user;
            $_SESSION['login'] = true;
            header("Location: work.php");
        }

        else{
          $statement = $conn->prepare("SELECT username FROM users WHERE username= ?");
          $statement->bindParam(1,$user);
          $statement->execute();
          $result = $statement->fetchAll();

          if($result == null){
            echo "<span class=\"error\">No user $user exists.</span>";
          }

          else{
            echo "<span class=\"error\">Incorrect password.</span>";
           }
      }
    }
}

    //register
    else {
      if(empty($user) || empty($pass)) {
      echo 'All field are required';
    }

    //successful registeration
    else{

      $conn = $con->openConnection();
      $statement = $conn->prepare("SELECT * FROM users WHERE username= ?");
      $statement->bindParam(1,$user);
      $statement->execute();
      $result = $statement->fetchAll();

      if(strlen($user) < 4 || strlen($user) > 31){
      echo "<span class=\"error\">Invalid
username: Must be between 4 and 31 characters long.”</span>";

}else if(!ctype_alnum($user)){
echo "<span class=\"error\">Invalid username: Must contain only numbers and letters.</span>";
}

else if(strlen($pass) < 8 || strlen($pass) > 31){
echo "<span class=\"error\">Invalid password: Must be between 8 and 31 characters long.</span>";
}

// check password at least one lowercase, uppercase and numeric character
else if(!preg_match("/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/", $pass)){
  echo "<span class=\"error\">Invalid password: Must contain lowercase
letters, uppercase letters, and numbers.</span>";

}else if($statement->rowCount() == 1){
  echo "<span class=\"error\">User $user already exists.</span>";

}else{
  $sessionclick = 0;
  $statement = $conn->prepare("INSERT INTO users VALUES (?,?,?)");
  $statement->bindParam(1,$user);
  $statement->bindParam(2,$pass);
  $statement->bindParam(3, $sessionclick);
  $statement->execute();

  echo "<span class=\"create\">Account $user created!</span>";

}

}
}
}

}else{

  if (isset($_SESSION['from2']) && $_SESSION['from2']) {

unset($_SESSION['from2']);
    echo "<span class=\"create\">You have been logged out!</span>";
session_destroy();
exit;

  }
  //
  // if(!isset($_SESSION['user']))
  //    {
  //     //  header("Location: login.php");
  //     echo "<span class=\"create\">You have been logged out!</span>";
  //       //use this if you only want the user_id session to be unset.
  //    }
}
?>

<!-- <html>

<head>
  <title>Work Website</title>
  <style>
    body {
      text-align: center;
    }

    p {
      color: red;
    }
  </style>
</head>

<body>
  <h1> Welcome to the Work Website</h1>
  <form action="login.php" method="post">
    Username: <input type="text" name="user" id="user"><br><br>
    Password: <input type="password" name="pass" id="pass"><br><br>
    <input type="submit" name="login" id="login" value="Login" /><br><br>
    <input type="submit" name="register" id="register" value="Register" />

  </form>

</body>

</html>

</body>
</html> -->

<!-- // if($statement->rowCount() == 1){
//   echo "User verified";
// }
// else if($user != $_POST['user']){
//   echo "No user $user exists";
// } -->
