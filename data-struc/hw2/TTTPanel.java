
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * File: TTTPanel.java Author: Foong Min Wong Description: This class makes the
 * tic-tac-toe panel The user is allowed to select a one-player version or a
 * two-player version. One-player version is not implemented Two-player version
 * is turn based. First X marks a square then O moves.
 */

public class TTTPanel extends JPanel {
	private String turn; // keep track if it is X or O's turn
	private boolean onePlay; // keep track of one player or two player

	private JPanel topPanel; // panel to hold radio buttons and info
	private JRadioButton onePlayer; // for user to select if they want to play
									// against the computer
	private JRadioButton twoPlayer; // for user to select if they want to play
									// against another human
	private ButtonGroup group; // for mutual exclusion of player selection
	private JLabel info; // to communicate instructions to user

	private JPanel panel; // panel for buttons for squares
	private JButton upperR; // upper right corner
	private JButton upperM; // upper middle
	private JButton upperL; // upper left corner
	private JButton middleR; // middle right
	private JButton middleM; // middle of board
	private JButton middleL; // middle left
	private JButton lowerR; // lower right corner
	private JButton lowerM; // lower middle
	private JButton lowerL; // lower left corner

	private JButton reset; // button to reset board

	private Font font1 = new Font("Courier", Font.BOLD, 50);// so X and O are
															// larger

	private JPanel mainPanel; // panel to hold topPanel and
								// onePlayerPanel/twoPlayerPanel
	private JPanel onePlayerPanel; // panel for one player version only
	private JPanel twoPlayerPanel;// panel for two player version only
	private JButton submit1;// submit name button for one player version
	private JButton submit2;// submit names button for two player version
	private JLabel name; // to instruct users to insert their names

	private JLabel nameX; // to instruct playerX's name
	private JLabel nameO; // to instruct playerO's name

	private JTextField string; // textfield to insert one player's name
	private JTextField stringX; // textfield to insert playerX's name
	private JTextField stringO;// textfield to insert playerO's name

	/**
	 * Constructor that initializes all the components for GUI
	 *
	 */
	public TTTPanel() {

		// set the layout for the panel that will hold all the game components
		setLayout(new BorderLayout());

		// make radio buttons for player options
		onePlayer = new JRadioButton("One Player");
		twoPlayer = new JRadioButton("Two Player");

		// make the radio buttons mutual exclusive
		group = new ButtonGroup();
		group.add(onePlayer);
		group.add(twoPlayer);

		// Add item listeners to radio buttons so we can tell when they have
		// been selected
		ItemListener oneplay = new RadioOnePlayer();
		onePlayer.addItemListener(oneplay);
		ItemListener twoplay = new RadioTwoPlayer();
		twoPlayer.addItemListener(twoplay);

		// main panel consists of topPanel and onePlayerPanel(one-player
		// version)
		// anotherPlayer2 (two-player version)
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));

		// initialize top panel that will hold radio buttons
		topPanel = new JPanel();
		// set the layout so things will be added horizontally
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.LINE_AXIS));

		// add radio buttons
		topPanel.add(onePlayer);
		topPanel.add(twoPlayer);

		// initialize info and make the text red
		info = new JLabel("Select how many players");
		info.setForeground(Color.red);

		// add the info
		topPanel.add(info);
		// add the top panel to the top of the TTT panel
		mainPanel.add(topPanel);

		// Appear in One Player Version Only
		onePlayerPanel = new JPanel();
		onePlayerPanel.setLayout(new BoxLayout(onePlayerPanel, BoxLayout.LINE_AXIS));

		// Your name
		name = new JLabel("Your name:");
		onePlayerPanel.add(name);

		// Textfield for name
		string = new JTextField(15);
		onePlayerPanel.add(string);

		// Submit button 1
		submit1 = new JButton("Submit Name");
		// ActionListener for submit1
		ActionListener submit1Action = new Submit1();
		submit1.addActionListener(submit1Action);
		// add submit button to the panel
		onePlayerPanel.add(submit1);

		// Appear in Two Player version only
		twoPlayerPanel = new JPanel();
		twoPlayerPanel.setLayout(new BoxLayout(twoPlayerPanel, BoxLayout.LINE_AXIS));

		// Player X's name
		nameX = new JLabel("Player X's name:");
		twoPlayerPanel.add(nameX);

		// Textfield for Player's X name
		stringX = new JTextField(15);
		twoPlayerPanel.add(stringX);

		// Player O's name
		nameO = new JLabel("Player O's name:");
		twoPlayerPanel.add(nameO);

		// Textfield for Player's O name
		stringO = new JTextField(15);
		twoPlayerPanel.add(stringO);

		// Submit button 2
		submit2 = new JButton("Submit Names");
		// ActionListener for submit2
		ActionListener submit2Action = new Submit2();
		submit2.addActionListener(submit2Action);
		// add submit button to the panel
		twoPlayerPanel.add(submit2);

		// add panel to the north of TTTPanel
		add(mainPanel, BorderLayout.NORTH);

		// initialize the reset button
		reset = new JButton("Reset");
		// action listener for reset
		ActionListener resetBoard = new ResetAction();
		reset.addActionListener(resetBoard);
		// add reset button to bottom of the TTT panel
		add(reset, BorderLayout.SOUTH);

		// action listeners for all the squares
		ActionListener takeSquare = new TakeSquareAction();

		// add square buttons in 3x3 grid
		panel = new JPanel();
		// this sets the layout to 3x3 with a 5 pixel border
		// that goes around the grid cells
		panel.setLayout(new GridLayout(3, 3, 5, 5));

		// for all buttons, set them to blank text
		// add action listener then
		// add to panel
		upperL = new JButton(" ");
		upperL.setFont(font1);
		upperL.addActionListener(takeSquare);
		panel.add(upperL);

		upperM = new JButton(" ");
		upperM.setFont(font1);
		upperM.addActionListener(takeSquare);
		panel.add(upperM);

		upperR = new JButton(" ");
		upperR.setFont(font1);
		upperR.addActionListener(takeSquare);
		panel.add(upperR);

		middleL = new JButton(" ");
		middleL.setFont(font1);
		middleL.addActionListener(takeSquare);
		panel.add(middleL);

		middleM = new JButton(" ");
		middleM.setFont(font1);
		middleM.addActionListener(takeSquare);
		panel.add(middleM);

		middleR = new JButton(" ");
		middleR.setFont(font1);
		middleR.addActionListener(takeSquare);
		panel.add(middleR);

		lowerL = new JButton(" ");
		lowerL.setFont(font1);
		lowerL.addActionListener(takeSquare);
		panel.add(lowerL);

		lowerM = new JButton(" ");
		lowerM.setFont(font1);
		lowerM.addActionListener(takeSquare);
		panel.add(lowerM);

		lowerR = new JButton(" ");
		lowerR.setFont(font1);
		lowerR.addActionListener(takeSquare);
		panel.add(lowerR);

		// add the grid panel to the center of the TTTPanel
		add(panel, BorderLayout.CENTER);
		disableAllButtons();// make sure the user selects one or two player
							// first

	}

	/**
	 * RadioOnePlayer class sets the game up for one player
	 */
	private class RadioOnePlayer implements ItemListener {

		public void itemStateChanged(ItemEvent e) {
			// set onePlay
			onePlay = true;
			// disable two player option
			twoPlayer.setEnabled(false);
			// you don't want people to switch versions.

			// set info to say it's Your turn then start the one player game
			info.setText("Enter your name.");
			disableAllButtons();

			// create a panel
			// only add onePlayerPanel for 1-player version only
			mainPanel.add(onePlayerPanel);

			// startGame();

		}
	}

	/**
	 * This class will get the one player's name. If the user doesn't insert any
	 * names, a message will be generated to tell the user to input name so that
	 * they could start the game.
	 */
	private class Submit1 implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			// check if user puts blank for the player's name
			if (string.getText().isEmpty() || string.getText().trim().equals("")) {
				// inform users to insert name
				JOptionPane.showMessageDialog(null, "You need to enter your name!");

			} else {// else indicate the player's turn to start
				info.setText(string.getText() + "'s turn");
				// prevent player from changing another name
				string.setEnabled(false);
				// prevent user from submitting name
				submit1.setEnabled(false);
				// start game after player inserts his/her name
				startGame();
			}
		}
	}

	/**
	 * RadioTwoPlayer class sets the game up for two players It will be turn
	 * based. X is first.
	 */
	private class RadioTwoPlayer implements ItemListener {
		// has to be here, if not, if won't compile
		public void itemStateChanged(ItemEvent e) {
			// set the turn to X
			turn = "X";
			// set onePlay to false
			onePlay = false;
			// they cannot go to one player now

			// disable one player option
			onePlayer.setEnabled(false);

			// set info to say it's X's turn then start the two player game
			info.setText("Enter player names");

			// only add twoPlayerPanel for 2-player version only
			mainPanel.add(twoPlayerPanel);

		}
	}

	/**
	 * This class will get two players'names.If the users don't insert any
	 * names, a message will be generated to tell either Player X or Player O to
	 * input name so that they could start the game.
	 */
	private class Submit2 implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			// check if playerX puts blank for the player's name
			if (stringX.getText().isEmpty() || stringX.getText().trim().equals("")) {
				// inform playerX to insert name
				JOptionPane.showMessageDialog(null, "Player X needs a name!");

			}

			// check if playerO puts blank for the player's name
			if (stringO.getText().isEmpty() || stringO.getText().trim().equals("")) {
				// inform playerO to insert name
				JOptionPane.showMessageDialog(null, "Player O needs a name!");
			}

			// check if names are fileld for both textfields
			if (!(stringX.getText().isEmpty() || stringX.getText().trim().equals(""))
					&& !(stringO.getText().isEmpty() || stringO.getText().trim().equals(""))) {

				// indicate playerX starts first
				info.setText(stringX.getText() + "'s turn");
				// prevent playerX from changing another name
				stringX.setEnabled(false);
				// prevent playerO from changing another name
				stringO.setEnabled(false);
				// prevent players from submitting another names
				submit2.setEnabled(false);
				// start game
				startGame();
			}

		}
	}

	/**
	 * This class will reset the text for all the buttons to a blank and enable
	 * them all again so they can be clicked
	 */
	private class ResetAction implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			// set text to blank for all buttons and color to white
			// upperleft
			upperL.setText(" ");
			upperL.setBackground(Color.WHITE);

			upperM.setText(" ");
			upperM.setBackground(Color.WHITE);

			upperR.setText(" ");
			upperR.setBackground(Color.WHITE);

			// middleleft
			middleL.setText(" ");
			middleL.setBackground(Color.WHITE);

			middleM.setText(" ");
			middleM.setBackground(Color.WHITE);

			middleR.setText(" ");
			middleR.setBackground(Color.WHITE);

			// lowerleft
			lowerL.setText(" ");
			lowerL.setBackground(Color.WHITE);

			lowerM.setText(" ");
			lowerM.setBackground(Color.WHITE);

			lowerR.setText(" ");
			lowerR.setBackground(Color.WHITE);

			// clear all of the player options
			group.clearSelection();
			// enable player options
			// you want to play again, just reset
			onePlayer.setEnabled(true);
			twoPlayer.setEnabled(true);

			// remove onePlayerPanel
			mainPanel.remove(onePlayerPanel);
			// remove twoPlayerPanel
			mainPanel.remove(twoPlayerPanel);
			// set string textfield blank
			string.setText(" ");
			// string.setEditable(true);
			string.setEnabled(true);
			submit1.setEnabled(true);

			// set stringXtextfield blank
			stringX.setText(" ");
			stringX.setEditable(true);
			stringX.setEnabled(true);

			// set stringOtextfield blank
			stringO.setText(" ");
			stringO.setEditable(true);
			stringO.setEnabled(true);
			submit2.setEnabled(true);

			// set info text
			info.setText("Select how many players");

			// make it so the user has to select player option first disable
			// buttons
			disableAllButtons();

		}

	}

	/**
	 * TakeSquareAction class When square buttons are clicked - it will mark the
	 * square appropriately. Appropriate moves are based on if it is two player
	 * or one player.
	 */
	private class TakeSquareAction implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			/*
			 * 
			 * TWO PLAYER VERSION
			 * 
			 * 
			 */
			// two player option
			if (!onePlay) {
				// get the button that was pressed
				JButton b = (JButton) event.getSource(); // pass the event

				// X's turn
				if (turn.equals("X")) {
					// set the button text to X
					b.setText("X");
					b.setBackground(Color.BLUE);
					// disable the button
					b.setEnabled(false);
					// check if it won the game
					if (checkForWin()) {
						info.setText(stringX.getText() + " is the winner!");
						disableAllButtons(); // stop the game by disabling the
												// buttons
					}
					// else check for a draw
					else {
						// if game is not over - it is O's turn
						if (checkForDraw()) {
							info.setText("Draw.");
						} else {
							turn = "O";
							// indicate Player 1's turn
							info.setText("It's " + stringO.getText() + "'s turn");
						}
					}

				}
				// O's turn
				else {
					// set the button text to O
					b.setText("O");
					b.setBackground(Color.GREEN);
					// disable the button
					b.setEnabled(false);
					// check if it won the game
					if (checkForWin()) {
						info.setText(stringO.getText() + " is the winner!");
						disableAllButtons();
					}
					// else check for a draw
					else {
						if (checkForDraw()) {
							info.setText("Draw.");
						} else {
							// if game is not over - it is X's turn
							turn = "X";
							// indicate Player2 turn
							info.setText("It's " + stringX.getText() + "'s turn");
						}
					}

				}
			}

			/*
			 * 
			 * ONE PLAYER VERSION
			 * 
			 */

			// one player option
			else {

				/* You need to create code for your counter move */
				JButton b = (JButton) event.getSource();
				turn = "X";

				if (turn.equals("X")) {
					// set the button text to X
					b.setText("X");
					b.setBackground(Color.BLUE);
					// disable the button
					b.setEnabled(false);
					// check if it won the game
					if (checkForWin()) {
						info.setText(string.getText() + " is the winner!");
						disableAllButtons(); // stop the game by disabling the
												// buttons
					}
					// else check for a draw
					else {
						// if game is not over - it is O's turn
						if (checkForDraw()) {
							info.setText("Draw.");
						} else {
							turn = "O";

							// Following are the counter moves of computer
							// that tackle different conditions of X

							// Opponent: (Cross) when X is on 2 squares
							if ((upperL.getText().equals("X") && lowerR.getText().equals("X"))
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if ((upperR.getText().equals("X") && lowerL.getText().equals("X"))
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if ((upperL.getText().equals("X") && middleM.getText().equals("X"))
									&& lowerR.getText().equals(" ")) {
								turn = "O";
								lowerR.setText("O");
								lowerR.setBackground(Color.GREEN);
								lowerR.setEnabled(false);
							}

							else if ((upperR.getText().equals("X") && middleM.getText().equals("X"))
									&& lowerL.getText().equals(" ")) {
								turn = "O";
								lowerL.setText("O");
								lowerL.setBackground(Color.GREEN);
								lowerL.setEnabled(false);
							}

							else if ((lowerL.getText().equals("X") && middleM.getText().equals("X"))
									&& upperR.getText().equals(" ")) {
								turn = "O";
								upperR.setText("O");
								upperR.setBackground(Color.GREEN);
								upperR.setEnabled(false);
							}

							else if ((lowerR.getText().equals("X") && middleM.getText().equals("X"))
									&& upperL.getText().equals(" ")) {
								turn = "O";
								upperL.setText("O");
								upperL.setBackground(Color.GREEN);
								upperL.setEnabled(false);
							}
							// Opponent: when X is in on 2 squares horizontally
							// and middle is blank
							else if (upperL.getText().equals("X") && upperR.getText().equals("X")
									&& (upperM.getText().equals(" "))) {
								turn = "O";
								upperM.setText("O");
								upperM.setBackground(Color.GREEN);
								upperM.setEnabled(false);
							} else if (middleL.getText().equals("X") && middleR.getText().equals("X")
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							} else if (lowerL.getText().equals("X") && lowerR.getText().equals("X")
									&& lowerM.getText().equals(" ")) {
								turn = "O";
								lowerM.setText("O");
								lowerM.setBackground(Color.GREEN);
								lowerM.setEnabled(false);
							}

							// Opponent: when X is 2 squares vertically
							else if (upperL.getText().equals("X") && lowerL.getText().equals("X")
									&& middleL.getText().equals(" ")) {
								turn = "O";
								middleL.setText("O");
								middleL.setBackground(Color.GREEN);
								middleL.setEnabled(false);
							} else if (upperM.getText().equals("X") && lowerM.getText().equals("X")
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							} else if (upperR.getText().equals("X") && lowerR.getText().equals("X")
									&& middleR.getText().equals(" ")) {
								turn = "O";
								middleR.setText("O");
								middleR.setBackground(Color.GREEN);
								middleR.setEnabled(false);
							}

							// Opponent: A Special Counter Move
							else if (upperM.getText().equals("X") && middleL.getText().equals("X")
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if (upperM.getText().equals("X") && middleR.getText().equals("X")
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if (lowerM.getText().equals("X") && middleL.getText().equals("X")
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if (lowerM.getText().equals("X") && middleR.getText().equals("X")
									&& middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if (middleM.getText().equals("X") && lowerR.getText().equals("X")
									&& middleR.getText().equals(" ")) {
								turn = "O";
								middleR.setText("O");
								middleR.setBackground(Color.GREEN);
								middleR.setEnabled(false);

							}

							// Opponent: when X has 2 squares horizontally
							else if ((upperL.getText().equals("X") && upperM.getText().equals("X"))
									&& upperR.getText().equals(" ")) {
								turn = "O";
								upperR.setText("O");
								upperR.setBackground(Color.GREEN);
								upperR.setEnabled(false);
							}

							else if ((middleL.getText().equals("X") && middleM.getText().equals("X"))
									&& middleR.getText().equals(" ")) {
								turn = "O";
								middleR.setText("O");
								middleR.setBackground(Color.GREEN);
								middleR.setEnabled(false);
							}

							else if ((lowerL.getText().equals("X") && lowerM.getText().equals("X"))
									&& lowerR.getText().equals(" ")) {
								turn = "O";

								lowerR.setText("O");
								lowerR.setBackground(Color.GREEN);
								lowerR.setEnabled(false);

							}

							// Opponent: when X is on two squares horizontally
							else if ((upperM.getText().equals("X") && upperR.getText().equals("X"))
									&& upperL.getText().equals(" ")) {
								turn = "O";
								upperL.setText("O");
								upperL.setBackground(Color.GREEN);
								upperL.setEnabled(false);
							}

							else if ((middleM.getText().equals("X") && middleR.getText().equals("X"))
									&& middleL.getText().equals(" ")) {
								turn = "O";
								middleL.setText("O");
								middleL.setBackground(Color.GREEN);
								middleL.setEnabled(false);
							}

							else if ((lowerM.getText().equals("X") && lowerR.getText().equals("X"))
									&& lowerL.getText().equals(" ")) {
								turn = "O";
								lowerL.setText("O");
								lowerL.setBackground(Color.GREEN);
								lowerL.setEnabled(false);
							}

							// Opponent: when X is in on the upper and lower
							// squares vertically
							else if ((upperL.getText().equals("X") && middleL.getText().equals("X"))
									&& lowerL.getText().equals(" ")) {
								turn = "O";
								lowerL.setText("O");
								lowerL.setBackground(Color.GREEN);
								lowerL.setEnabled(false);
							}

							else if ((upperM.getText().equals("X") && middleM.getText().equals("X"))
									&& lowerM.getText().equals(" ")) {
								turn = "O";
								lowerM.setText("O");
								lowerM.setBackground(Color.GREEN);
								lowerM.setEnabled(false);
							} else if ((upperR.getText().equals("X") && middleR.getText().equals("X"))
									&& lowerR.getText().equals(" ")) {
								turn = "O";
								lowerR.setText("O");
								lowerR.setBackground(Color.GREEN);
								lowerR.setEnabled(false);
							}

							// Opponent: when X is in on the lower and middle
							// squares vertically
							else if ((lowerL.getText().equals("X") && middleL.getText().equals("X"))
									&& upperL.getText().equals(" ")) {
								turn = "O";
								upperL.setText("O");
								upperL.setBackground(Color.GREEN);
								upperL.setEnabled(false);
							}

							else if ((lowerM.getText().equals("X") && middleM.getText().equals("X"))
									&& upperM.getText().equals(" ")) {
								turn = "O";
								upperM.setText("O");
								upperM.setBackground(Color.GREEN);
								upperM.setEnabled(false);
							}

							else if ((lowerR.getText().equals("X") && middleR.getText().equals("X"))
									&& upperR.getText().equals(" ")) {
								turn = "O";
								upperR.setText("O");
								upperR.setBackground(Color.GREEN);
								upperR.setEnabled(false);
							}

							// Opponent: when X is on the corner
							else if ((upperL.getText().equals("X")) && middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if ((upperR.getText().equals("X")) && middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if ((lowerL.getText().equals("X")) && middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if ((lowerR.getText().equals("X")) && middleM.getText().equals(" ")) {
								turn = "O";
								middleM.setText("O");
								middleM.setBackground(Color.GREEN);
								middleM.setEnabled(false);
							}

							else if ((upperL.getText().equals("X")) && upperM.getText().equals(" ")) {
								turn = "O";
								upperM.setText("O");
								upperM.setBackground(Color.GREEN);
								upperM.setEnabled(false);
							}

							else if ((upperL.getText().equals("X")) && middleL.getText().equals(" ")) {
								turn = "O";
								middleL.setText("O");
								middleL.setBackground(Color.GREEN);
								middleL.setEnabled(false);
							}

							else if ((upperR.getText().equals("X")) && upperM.getText().equals(" ")) {
								turn = "O";
								upperM.setText("O");
								upperM.setBackground(Color.GREEN);
								upperM.setEnabled(false);
							}

							else if ((upperR.getText().equals("X")) && middleR.getText().equals(" ")) {
								turn = "O";
								middleR.setText("O");
								middleR.setBackground(Color.GREEN);
								middleR.setEnabled(false);
							}

							else if ((lowerL.getText().equals("X")) && lowerM.getText().equals(" ")) {
								turn = "O";
								lowerM.setText("O");
								lowerM.setBackground(Color.GREEN);
								lowerM.setEnabled(false);
							}

							else if ((lowerL.getText().equals("X")) && middleL.getText().equals(" ")) {
								turn = "O";
								lowerL.setText("O");
								lowerL.setBackground(Color.GREEN);
								lowerL.setEnabled(false);
							}

							else if ((lowerR.getText().equals("X")) && lowerM.getText().equals(" ")) {
								turn = "O";
								lowerM.setText("O");
								lowerM.setBackground(Color.GREEN);
								lowerM.setEnabled(false);
							}

							else if ((lowerR.getText().equals("X")) && middleR.getText().equals(" ")) {
								turn = "O";
								middleR.setText("O");
								middleR.setBackground(Color.GREEN);
								middleR.setEnabled(false);
							}

							// Opponent: when X is on edge
							else if ((upperM.getText().equals("X")) && upperL.getText().equals(" ")) {
								turn = "O";
								upperL.setText("O");
								upperL.setBackground(Color.GREEN);
								upperL.setEnabled(false);
							}

							else if ((upperM.getText().equals("X")) && upperR.getText().equals(" ")) {
								turn = "O";
								upperR.setText("O");
								upperR.setBackground(Color.GREEN);
								upperR.setEnabled(false);
							}

							else if ((lowerM.getText().equals("X")) && lowerL.getText().equals(" ")) {
								turn = "O";
								lowerL.setText("O");
								lowerL.setBackground(Color.GREEN);
								lowerL.setEnabled(false);
							}

							else if ((lowerM.getText().equals("X")) && lowerR.getText().equals(" ")) {
								turn = "O";
								lowerR.setText("O");
								lowerR.setBackground(Color.GREEN);
								lowerR.setEnabled(false);
							}

							else if ((middleL.getText().equals("X")) && upperL.getText().equals(" ")) {
								turn = "O";
								upperL.setText("O");
								upperL.setBackground(Color.GREEN);
								upperL.setEnabled(false);
							}

							else if ((middleL.getText().equals("X")) && lowerL.getText().equals(" ")) {
								turn = "O";
								lowerL.setText("O");
								lowerL.setBackground(Color.GREEN);
								lowerL.setEnabled(false);
							}

							else if ((middleR.getText().equals("X")) && upperR.getText().equals(" ")) {
								turn = "O";
								upperR.setText("O");
								upperR.setBackground(Color.GREEN);
								upperR.setEnabled(false);
							}

							else if ((middleR.getText().equals("X")) && lowerR.getText().equals(" ")) {
								turn = "O";
								lowerR.setText("O");
								lowerR.setBackground(Color.GREEN);
								lowerR.setEnabled(false);
							}

							// Opponent: when X is on the middle
							else if (middleM.getText().equals("X")) {
								if (upperL.getText().equals(" ")) {
									turn = "O";
									upperL.setText("O");
									upperL.setBackground(Color.GREEN);
									upperL.setEnabled(false);
								} else if (upperR.getText().equals(" ")) {
									turn = "O";
									upperR.setText("O");
									upperR.setBackground(Color.GREEN);
									upperR.setEnabled(false);
								} else if (lowerL.getText().equals(" ")) {
									turn = "O";
									lowerL.setText("O");
									lowerL.setBackground(Color.GREEN);
									lowerL.setEnabled(false);
								} else if (lowerR.getText().equals(" ")) {
									turn = "O";
									lowerR.setText("O");
									lowerR.setBackground(Color.GREEN);
									lowerR.setEnabled(false);
								}

							}

							// check if there is any blank, then O will take it
							else if (upperM.getText().equals(" ")) {
								turn = "O";
								upperM.setText("O");
								upperM.setBackground(Color.GREEN);
								upperM.setEnabled(false);
							} else if (upperL.getText().equals(" ")) {
								turn = "O";
								upperL.setText("O");
								upperL.setBackground(Color.GREEN);
								upperL.setEnabled(false);
							} else if (upperR.getText().equals(" ")) {
								turn = "O";
								upperR.setText("O");
								upperR.setBackground(Color.GREEN);
								upperR.setEnabled(false);
							} else if (lowerL.getText().equals(" ")) {
								turn = "O";
								lowerL.setText("O");
								lowerL.setBackground(Color.GREEN);
								lowerL.setEnabled(false);
							} else if (lowerM.getText().equals(" ")) {
								turn = "O";
								lowerM.setText("O");
								lowerM.setBackground(Color.GREEN);
								lowerM.setEnabled(false);
							} else if (lowerR.getText().equals(" ")) {
								turn = "O";
								lowerR.setText("O");
								lowerR.setBackground(Color.GREEN);
								lowerR.setEnabled(false);
							} else if (upperL.getText().equals(" ")) {
								turn = "O";
								upperL.setText("O");
								upperL.setBackground(Color.GREEN);
								upperL.setEnabled(false);
							} else if (upperM.getText().equals(" ")) {
								turn = "O";
								upperM.setText("O");
								upperM.setBackground(Color.GREEN);
								upperM.setEnabled(false);
							} else if (upperR.getText().equals(" ")) {
								turn = "O";
								upperR.setText("O");
								upperR.setBackground(Color.GREEN);
								upperR.setEnabled(false);
							}
						}
					}

				}

				// check if O won the game
				if (checkForWin()) {
					info.setText(" Computer is the winner!");
					disableAllButtons();
				}
				// else check for a draw
				else {
					if (checkForDraw()) {
						info.setText("Draw.");
					} else {
						// if game is not over - it is X's turn
						turn = "X";
						// indicate Player2 turn
						info.setText("It's " + string.getText() + "'s turn");
					}
				}

			}
		}
	}

	// }

	/**
	 * checkForWin This goes through all the options for a three in a row win
	 * 
	 * @return true if there are three X's or three O's in a row, otherwise
	 *         false
	 */
	public boolean checkForWin() {
		if (((upperR.getText().equals(upperM.getText())) && (upperR.getText().equals(upperL.getText()))
				&& (!upperR.getText().equals(" ")))
				|| ((middleR.getText().equals(middleM.getText())) && (middleR.getText().equals(middleL.getText()))
						&& (!middleR.getText().equals(" ")))
				|| ((lowerR.getText().equals(lowerM.getText())) && (lowerR.getText().equals(lowerL.getText()))
						&& (!lowerR.getText().equals(" ")))
				|| ((upperR.getText().equals(middleR.getText())) && (upperR.getText().equals(lowerR.getText()))
						&& (!upperR.getText().equals(" ")))
				|| ((upperM.getText().equals(middleM.getText())) && (upperM.getText().equals(lowerM.getText()))
						&& (!upperM.getText().equals(" ")))
				|| ((upperL.getText().equals(middleL.getText())) && (upperL.getText().equals(lowerL.getText()))
						&& (!upperL.getText().equals(" ")))
				|| ((upperR.getText().equals(middleM.getText())) && (upperR.getText().equals(lowerL.getText()))
						&& (!upperR.getText().equals(" ")))
				|| ((upperL.getText().equals(middleM.getText())) && (upperL.getText().equals(lowerR.getText()))
						&& (!upperL.getText().equals(" ")))) {
			return true;
		} else
			return false;

	}

	/**
	 * checkForDraw This goes through and sees if all the squares are not blank.
	 * If all the squares are marked then it is a draw
	 * 
	 * @return true if none of the squares are blank.
	 */
	public boolean checkForDraw() {
		if (!upperR.getText().equals(" ") && !upperM.getText().equals(" ") && !upperL.getText().equals(" ")
				&& !middleR.getText().equals(" ") && !middleM.getText().equals(" ") && !middleL.getText().equals(" ")
				&& !lowerR.getText().equals(" ") && !lowerM.getText().equals(" ") && !lowerL.getText().equals(" ")) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * disableAllButtons Sets all of the square buttons to false so the user
	 * cannot click them
	 */
	public void disableAllButtons() {
		upperR.setEnabled(false);
		upperM.setEnabled(false);
		upperL.setEnabled(false);
		middleR.setEnabled(false);
		middleM.setEnabled(false);
		middleL.setEnabled(false);
		lowerR.setEnabled(false);
		lowerM.setEnabled(false);
		lowerL.setEnabled(false);
	}

	/**
	 * startGame Enables all of the square buttons
	 */
	public void startGame() {
		upperR.setEnabled(true);
		upperM.setEnabled(true);
		upperL.setEnabled(true);
		middleR.setEnabled(true);
		middleM.setEnabled(true);
		middleL.setEnabled(true);
		lowerR.setEnabled(true);
		lowerM.setEnabled(true);
		lowerL.setEnabled(true);
	}

}
