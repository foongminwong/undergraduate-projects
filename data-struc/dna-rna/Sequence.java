/**
 *   Sequence Class: contain constructors and other Sequence methods
 */

/**
 * @author Foong Min Wong
 *
 */
public class Sequence {
	private String type;
	private SLList bases;
	private int sizeOfBases;
	private int position;

	// constructor for a sequence
	public Sequence(String type, SLList bases) {
		this.type = type;
		this.bases = bases;
	}

	// another constructor for a sequence
	public Sequence(int position, String type, SLList bases) {
		this.position = position;
		this.type = type;
		this.bases = bases;
	}

	
	public Sequence(String type) {
		this.type = type;
	}

	// getetrs for types
	public String getType() {
		return this.type;
	}

	@SuppressWarnings("unchecked")
	// getters for bases
	public SLList<String> getBases() {
		return this.bases;
	}

	//setters for type
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * This method does the conversion and complement of letters sequence.
	 * 
	 * 
	 * @param bases
	 *            a String type SLList
	 */
	public void transcribeProcess(SLList<String> bases) {

		//initialize length of bases
		int lengthOfBases = bases.length();

		for (int i = 0; i < lengthOfBases; i++) {
			
			// if the base is C the set it to G
			if (bases.getValue(i).equals("C")) {
				bases.getNode(i).setElement("G");
			}

			// if the base is T the set it to A
			else if (bases.getValue(i).equals("T")) {
				bases.getNode(i).setElement("A");
			}

			// if the base is A the set it to U
			else if (bases.getValue(i).equals("A")) {
				bases.getNode(i).setElement("U");
			}

			// if the base is G the set it to C
			else if (bases.getValue(i).equals("G")) {
				bases.getNode(i).setElement("C");
			}
		}

	}
	
	
}
