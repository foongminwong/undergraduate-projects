
/**
 *   Singly Linked List: contain a list of SLList methods
 */

/**
 * @author Foong Min Wong
 *
 */
/***
 * SLList.java generic singly linked list class
 */

public class SLList<E> implements List<E> {

	private Node<E> head = null;
	private Node<E> tail = null;
	private int size;

	/** Constructors */
	public SLList(E item) {
		head = new Node<E>(item); // Create head
		size++;
	}

	public SLList() {
		head = null; // Create head
		size = 0;
	}

	/**
	 * inserts the given list after the given index
	 */
	public void insertList(SLList<E> list, int index) {

		Node node = new Node();
		// System.out.println(list.size + "first");//3
		// System.out.println(size + "second");//4
		// System.out.println(size + "here");
		if (index < 0 || index > size) {

			throw new IndexOutOfBoundsException(Integer.toString(index));
		}
		if (index == 0) {
			addFirst(list.getHead().getElement());
		} else {
			for (int i = 0; i < list.size; i++) {
				{
					insert(index + 1, list.getValue(i));
					index++;
				}
			}

		}

	}

	public Node<E> getHead() {
		return head;
	}

	public Node<E> getLast() {
		return getNode(size - 1);
	}

	/** Make a new head **/
	public void addFirst(E item) {

		head = new Node<E>(item, head); // create and link a new node
		if (size == 0) {
			tail = head; // special case: new node becomes tail also
		}

		size++;

	}

	/** walk the list converting data to Strings **/
	public String toString() {
		Node<E> nodeRef = head;
		String result = "";
		while (nodeRef != null) {
			result = result + nodeRef.getElement().toString();

			if (nodeRef.getNext() != null) {
				result = result + " ==> ";
			}
			nodeRef = nodeRef.getNext();
		}
		return result;
	}

	public String toString1() {
		Node<E> nodeRef = head;
		String result = "";
		while (nodeRef != null) {
			result = result + nodeRef.getElement().toString();

			if (nodeRef.getNext() != null) {
				result += result + "";
			}
			nodeRef = nodeRef.getNext();
		}
		return result;
	}

	public String toString2() {
		Node<E> nodeRef = head;
		String result = "";
		while (nodeRef != null) {
			result = result + nodeRef.getElement().toString();

			if (nodeRef.getNext() != null) {
				result = result + "";
			}
			nodeRef = nodeRef.getNext();
		}
		return result;
	}

	public String stringReplace() {
		Node<E> nodeRef = head;
		String result = "";
		while (nodeRef != null) {
			result = result + nodeRef.getElement().toString();

			if (nodeRef.getNext() != null) {
				result = result + "";
			}
			nodeRef = nodeRef.getNext();
		}

		result = result.replaceAll("T", "U");
		return result;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		head = null;
		tail = null;
		size = 0;

	}

	@Override
	public void insert(int index, E item) {
		// TODO Auto-generated method stub
		if (index < 0 || index > size) {
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}
		if (index == 0) {// first thing always has index 0
			addFirst(item);
		} else {
			// get the node before the index
			Node<E> node = getNode(index - 1);
			// add item after the node
			addAfter(node, item);
		}

	}

	@Override
	public void add(E item) {
		// TODO Auto-generated method stub
		if (size == 0) {// check if the the size is empty
			addFirst(item);
		} else {// if the list is not empty
			// get the last node
			Node<E> node = getNode(size - 1);
			// add item after the node
			addAfter(node, item);
		}

	}

	public Node<E> getNode(int index) {
		// TODO Auto-generated method stub

		if (head == null)// if there this no head
			throw new IndexOutOfBoundsException();

		// create a node as head
		Node<E> node = head;
		// traverse until item finds the node with that index
		for (int i = 0; i < index && node != null; i++) {
			// node will become the next node
			// list does not act like array to use indicate index to find
			// specific element
			node = node.getNext();
		}
		return node;

	}

	private void addAfter(Node<E> node, E item) {
		// TODO Auto-generated method stub
		node.setNext(new Node<E>(item, node.getNext()));
		size++;

	}

	@Override
	public void remove(int index) {
		// TODO Auto-generated method stub
		Node<E> curr = head;

		if (index == 0) {
			head = head.getNext();
		} else {
			for (int i = 1; i < index; i++) {
				curr = curr.getNext();
			}

			curr.setNext(curr.getNext().getNext());
		}

		size--;

	}

	@Override
	public E prev(int index) {
		// TODO Auto-generated method stub
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}

		Node<E> node = getNode(index - 1);

		return node.getElement();
	}

	@Override
	public E next(int index) {
		// TODO Auto-generated method stub
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}

		Node<E> node = getNode(index + 1);

		return node.getElement();
	}

	@Override
	public int length() {
		// TODO Auto-generated method stub
		return size;
	}

	/**
	 * reverses the list
	 */
	public void reverse() {
		Node<E> node = head;
		if (node == null || node.getNext() == null) {
			// empty or one node
			return;
		}
		Node<E> prev = node.getNext();
		Node<E> curr = prev.getNext();
		prev.setNext(node);
		node.setNext(null);

		while (curr != null) {
			// as soon as you get null, then you hit the end of your list
			Node<E> next = curr.getNext();
			curr.setNext(prev);
			prev = curr;
			curr = next;
		}
		head = prev;
		// System.out.println("This is not done yet");
	}

	@Override
	public E getValue(int index) {
		// TODO Auto-generated method stub
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}
		Node<E> node = getNode(index);
		return node.getElement();

	}

	//check whetehr it is empty
	public boolean isEmpty() {
		return size == 0;
	}

	//set the size to zero
	public void setZeroSize() {
		size = 0;
	}

	//set the tail to null
	public void setTailNextToNull(int index) {
		tail = getNode(index - 1);
		tail.setNext(null);
	}

	// get size
	public int getSize() {
		return size;
	}

	//set head
	public Node<E> setHead(int index) {
		Node<E> node = getNode(index);
		head = node;
		return head;
	}

	//remove first
	public void removeFirst() {
		remove(0);
	}

	//remove last
	public void removeLast() {
		remove(size - 1);
	}

}
