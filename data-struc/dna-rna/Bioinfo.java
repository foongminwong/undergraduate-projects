import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *   Program to model biological sequence
 */

/**
 * @author Foong Min Wong
 *
 */
public class Bioinfo {
	@SuppressWarnings({ "unchecked", "unused" })
	public static <E> void main(String[] args) throws FileNotFoundException {

		/** check if the number of argument is exactly 2 **/
		if (args.length == 2) {
			// create a Sequence array with a specific size passed by the 1st
			// argument
			Sequence[] sequences = new Sequence[Integer.parseInt(args[0])];

			// read in a file specified by the 2nd argument
			File file = new File(args[1]);
			Scanner in = new Scanner(file);

			// the strings for DNA
			String regexForDNA = "[ACGT]+";
			// the strings for RNA
			String regexForRNA = "[ACGU]+";

			// store length of sequence as lengthOfSequencess
			int lengthOfSequences = sequences.length;

			// create an SLLlist for bases
			SLList<String> StringListForBases = new SLList<String>();

			int count = 0;
			// initialize each sequences as empty
			while (count < lengthOfSequences) {
				sequences[count] = new Sequence("", StringListForBases);
				count++;
			}

			while (in.hasNextLine()) {
				// string is next line
				String str = in.nextLine();

				// split each strings by spaces
				String[] splitStr = str.split("\\s+");

				// create a Single LinkedList for the bases
				SLList<String> StringListForBase = new SLList<String>();

				/** If the command is insert */
				if (splitStr[0].equals("insert")) {

					// parse following the position string to integer
					int position = Integer.parseInt(splitStr[1]);

					// check for 4 argument
					if (splitStr.length == 4) {

						// identify the type
						String type = splitStr[2];

						// identify the string of bases
						String baseinString = splitStr[3];

						// call insert method
						insert(sequences, splitStr, StringListForBase, regexForDNA, regexForRNA);
					}

					// base is null
					else if (splitStr.length == 3) {
						// call insert method
						insert(sequences, splitStr, StringListForBase, regexForDNA, regexForRNA);
					}

					else {
						System.out.println("I need 4 argument for insert command.");
					}

				}

				/** if the command is remove */
				else if (splitStr[0].equals("remove")) {

					// check for 2 argument
					if (splitStr.length == 2) {
						// call remove method
						remove(sequences, splitStr);
					} else {
						System.out.println("I need 2 argument for remove command.");
					}

				}

				/** if the command is print and has 1 argument */
				else if (splitStr[0].equals("print") && splitStr.length == 1) {

					// check for 1 argument
					if (splitStr.length == 1) {
						// call print method
						print(sequences);
					}

					else {
						System.out.println("I need 1 argument for print command.");
					}
				}

				/**
				 * if the command is print specific position and has 2 argument
				 */
				else if (splitStr[0].equals("print") && splitStr.length == 2) {
					// parse the string position and store it as integer
					int position = Integer.parseInt(splitStr[1]);

					// check for 2 argument
					if (splitStr.length == 2) {

						// call printSpecificIndex method
						printSpecificIndex(sequences, position);
					} else {
						System.out.println("I need 2 argument for print pos command.");
					}
				}

				/**
				 * if the command is splice pos type bases start and has 5
				 * argument
				 * 
				 * @Purpose: Insert the given bases to the sequence at position
				 *           pos in the sequence array starting at the base of
				 *           the start position.
				 */
				else if (splitStr[0].equals("splice")) {

					// check for 5 argument
					if (splitStr.length == 5) {
						splice(sequences, splitStr, regexForDNA, regexForRNA);
					}

					else {
						System.out.println("I need 5 argument for print pos command.");
					}
				}

				/**
				 * if the command is clip
				 * 
				 * @ command 1: clip pos start end:
				 * 
				 * @ command 2:clip pos start:
				 */
				else if (splitStr[0].equals(("clip"))) {
					clip(sequences, splitStr);
				}

				/**
				 * if the command is copy
				 * 
				 * @command copy pos1 pos2
				 * 
				 */
				else if (splitStr[0].equals("copy")) {

					if (splitStr.length == 3) {
						copy(sequences, splitStr, args);
					} else {
						System.out.println("I need 2 argument for copy command");
					}
				}

				/**
				 * if command is swap
				 * 
				 * @command swap pos1 start1 pos2 start2
				 * 
				 */
				else if (splitStr[0].equals("swap")) {

					// if the number of argument is 5
					if (splitStr.length == 5) {
						swap(sequences, splitStr);

					} else {
						System.out.println("I need 5 argument for swap command.");
					}
				}

				/**
				 * if command is overlap
				 * 
				 * @command overlap pos1 pos2:
				 * 
				 */

				else if (splitStr[0].equals("overlap")) {
					// System.out.println(str);
					if (splitStr.length == 3) {
						overlap(sequences, splitStr);
					} else {
						System.out.println("I need 3 argument for overlap command.");
					}
				}

				/**
				 * if the command is transcribe
				 * 
				 * @command transcribe pos1: Transcription converts a DNA
				 *          sequence to an RNA sequence at pos1.
				 **/
				else if (splitStr[0].equals("transcribe")) {
					if (splitStr.length == 2) {
						transcribe(sequences, splitStr);
					} else {
						System.out.println("I need 2 argument for trasncribe command.");
					}

				}

				/**
				 * if the command is translation
				 * 
				 * @command transcribe pos1: Transcription converts a DNA
				 *          sequence to an RNA sequence at pos1.
				 **/
				else if (splitStr[0].equals("translate")) {
					if (splitStr.length == 2) {

						int position = Integer.parseInt(splitStr[1]);
						if (sequences[position].getType().equals("RNA")) {
							translate(sequences, splitStr);
						} else {
							System.out.println("To translate, we only accept DNA.");
						}

					} else {
						System.out.println("I need 2 argument for translate command.");
					}

				}

			}
		}
	}

	/**
	 * Insert sequence to position in the sequence array. The type will be
	 * either DNA or RNA.
	 * 
	 * @param sequences
	 *            the array of sequences
	 * @param spliStr
	 *            array that stores the split command
	 * @param StringListForBase
	 *            a singly linked list to store the bases
	 * @param regexForDNA
	 *            string that composed of A,C,G,T characters
	 * @param regexForRNA
	 *            string that composed of A,C,G,U characters
	 * 
	 * 
	 */

	public static void insert(Sequence[] sequences, String[] splitStr, SLList<String> StringListForBase,
			String regexForDNA, String regexForRNA) {
		// parse following the position string to integer
		int position = Integer.parseInt(splitStr[1]);

		// base is empty
		if (splitStr.length == 3) {

			// identify the type
			String type = splitStr[2];

			// identify the string of bases
			String baseinString = "";

			// check if the type equals DNA or RNA
			if (type.equals("DNA") || type.equals("RNA")) {

				// check if position is within 0-7 inclusive
				if (position >= 0 && position < sequences.length) {

				} else {
					// print message about invalid insert
					// position
					System.out.println("Invalid position to insert at position: " + position);
				}
			} else {

				// print message about incorrect bases
				System.out.println("Given type was not correct for given bases to insert at position " + position);
			}
		}

		// check if there are 4 tokens
		else if (splitStr.length == 4) {
			// if the first token is the insert command
			if (splitStr[0].equals("insert")) {

				// identify the type
				String type = splitStr[2];

				// identify the string of bases
				String baseinString = splitStr[3];

				// create a sequence
				// Sequence genes = new Sequence(type, StringListForBase);

				// check if the type equals DNA or RNA
				if (type.equals("DNA") || type.equals("RNA")) {

					// check if position is within 0-7 inclusive
					if (position >= 0 && position < sequences.length) {

						// if type is equal to DNA and check the
						// bases
						// only matches the form of DNA
						if (type.equals("DNA") && baseinString.matches(regexForDNA)) {

							// initialize dnaIndex as zero
							int dnaIndex = 0;

							// while dnaIndex less than length of
							// baseinString
							while (dnaIndex < baseinString.length()) {

								// insert each character to
								// specified dnaIndex
								StringListForBase.insert(dnaIndex, baseinString.charAt(dnaIndex) + "");

								// increment dnaIndex
								dnaIndex++;
							}

							// the sequence at specified position
							// will have a DNA type and SLList bases
							sequences[position] = new Sequence(type, StringListForBase);

						}

						// if type is equal to RNA and check the
						// bases
						// only matches the form of RNA
						else if (type.equals("RNA") && baseinString.matches(regexForRNA)) {

							// initialize rnaIndex as zero
							int rnaIndex = 0;

							// while rnaIndex less than length of
							// baseinString
							while (rnaIndex < baseinString.length()) {

								// insert each character to
								// specified rnaIndex
								StringListForBase.insert(rnaIndex, baseinString.charAt(rnaIndex) + "");

								// increment rnaIndex
								rnaIndex++;
							}

							// the sequence at specified position
							// will have a RNA type and SLList bases
							sequences[position] = new Sequence(type, StringListForBase);

						} else {

							// print message about incorrect bases
							System.out.println(
									"Given bases were not correct for given type to insert at position: " + position);

							// set the sequence at specified
							// position with empty type and empty
							// base
							sequences[position] = new Sequence("", null);

						}
					} else {
						// print message about invalid insert
						// position
						System.out.println("Invalid position to insert at position: " + position);
					}
				} else {

					// print message about incorrect bases
					System.out.println("Given type was not correct for given bases to insert at position " + position);
				}

			}
		} else {
			// print message about invalid insert position
			System.out.println("Invalid position to insert at position: " + position);
		}
	}

	/**
	 * Remove the sequence at position in the sequence array.If there is no
	 * sequence at position, output an error message.
	 * 
	 * 
	 * @param sequences
	 *            the array of sequences
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 * 
	 */
	public static void remove(Sequence[] sequences, String[] splitStr) {
		// re-check if the argument is 2
		if (splitStr.length == 2) {

			// parse the string position and store it as integer
			int position = Integer.parseInt(splitStr[1]);

			// check if the sequence is empty and print out error
			// message
			if (sequences[position].getBases().isEmpty()) {
				System.out.println("There is no sequence to remove at position: " + position);
			} else {

				// clear the bases in the specified position of
				// sequences
				sequences[position].getBases().clear();

				// set the type field to empty
				sequences[position] = new Sequence("", sequences[position].getBases());
			}
		}
	}

	/**
	 * Print the index and the type at position in the sequence array and the
	 * bases. If there is no sequence in that position, print an error message.
	 * 
	 * @param sequences
	 *            the array of sequences
	 * 
	 */
	public static void print(Sequence[] sequences) {
		// initialize i to 0
		int i = 0;

		// while i does not exceed the sequences array's length
		while (i < (sequences.length)) {

			// != null
			// if the type is empty
			if (sequences[i].getType().equals("") || sequences[i].getBases().equals(null)) {
				System.out.println(i + " " + " ");
			}
			// else if the type is null
			else if (sequences[i].getType().equals(null) || sequences[i].getBases().equals(null)) {
				System.out.println(i + " " + " ");
			}

			// else print out the index and the type at position in the sequence
			// array and the bases
			else {
				System.out.println(i + " " + sequences[i].getType() + " " + sequences[i].getBases());
			}

			i++;
		}
	}

	/**
	 * Print the index and the type at position pos in the sequence array and
	 * the bases. If there is no sequence in that position, print a suitable
	 * message.
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param i
	 *            the specific position of sequence
	 * 
	 */
	public static void printSpecificIndex(Sequence[] sequences, int i) {

		/*
		 * // if the type is empty or the base is empty if
		 * (sequences[i].getType().isEmpty() ||
		 * sequences[i].getBases().isEmpty()) {
		 * System.out.println("There is not sequence at position: " + i); }
		 */
		// else if the type is removed or base is null
		if (sequences[i].getType().equals("") || sequences[i].getBases().equals(null)) {
			System.out.println(i + " " + " ");
		}
		// if type and base are not empty
		else {
			System.out.println(i + " " + sequences[i].getType() + " " + sequences[i].getBases());
		}

	}

	/**
	 * Insert the given bases to the sequence at position pos in the sequence
	 * array starting at the base of the start position.
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 * @param regexForDNA
	 *            string that composed of A,C,G,T characters
	 * 
	 * @param regexForRNA
	 *            string that composed of A,C,G,U characters
	 */
	@SuppressWarnings("unchecked")
	public static void splice(Sequence[] sequences, String[] splitStr, String regexForDNA, String regexForRNA) {
		// if the number of argument is 5
		if (splitStr.length == 5) {
			// parse following string to integer
			int position = Integer.parseInt(splitStr[1]);

			// identify the type
			String type = splitStr[2];

			// identify the string of bases
			String baseinString = splitStr[3];

			// parse following string to integer
			int start = Integer.parseInt(splitStr[4]);

			if (position < 0 || position >= sequences.length - 1) {
				System.out.println("Invalid position to insert at position " + position);
			}
			// check if if the start position is not valid (the
			// start position is larger than the length of the
			// sequence).

			else if (start > sequences.length || start == 0 || start > sequences[position].getBases().getSize()) {
				System.out.println("Invalid position to insert at position: " + position);
			}

			else // check if there is no sequence at the position
			if (sequences[position].getBases().isEmpty()) {
				System.out.println("There is no sequence to splice at position: " + position);
			}

			// check if two sequences are not of the same type
			else if (!(sequences[position].getType().equals(type))) {
				System.out.println("Cannot splice if the types do not agree.");
			}

			// if type is equal to DNA and check the
			// bases
			// only matches the form of DNA
			else if (type.equals("DNA") && baseinString.matches(regexForDNA)) {

				// create a SLList for the string to be inserted
				SLList<String> StringListToBeInserted = new SLList<String>();

				// if the start position is same as the length of
				// bases
				if (start == sequences[position].getBases().length()) {

					// initialize dnaIndex to 0
					int dnaIndex = 0;

					// while dnaIndex is less than the length of
					// bases
					while (dnaIndex < baseinString.length()) {

						// append
						sequences[position].getBases().add(baseinString.charAt(dnaIndex) + "");

						// increment dnaIndex
						dnaIndex++;
					}

				}
				// else insert the list to a specific index
				else {

					// initialize dnaIndex to 0
					int dnaIndex = 0;

					// while dnaIndex is less than the length of
					// bases
					while (dnaIndex < baseinString.length()) {

						// insert the base to the specific index
						// you have to put + "" to turn baseinString.chartAt()
						// to a String
						StringListToBeInserted.insert(dnaIndex, baseinString.charAt(dnaIndex) + "");

						// increment dnaIndex
						dnaIndex++;
					}

					// insert the list from the start index
					sequences[position].getBases().insertList(StringListToBeInserted, start - 1);
				}

			} else if (type.equals("RNA") && baseinString.matches(regexForRNA)) {

				// create a new SLList for string to be inserted
				SLList<String> StringListToBeInserted = new SLList<String>();

				// initialize rnaCount to 0
				int rnaCount = 0;

				// while rnaCount is less than the
				while (rnaCount < baseinString.length()) {
					StringListToBeInserted.insert(rnaCount, baseinString.charAt(rnaCount) + "");

					// increment rnaCount
					rnaCount++;
				}

				sequences[position].getBases().insertList(StringListToBeInserted, start - 1);

			} else {
				// print out message that the abses are invalid
				System.out.println("The sequence you want to splice in is not made of valid bases.");
			}
		}
	}

	/**
	 * Replace the sequence at position pos with a clipped version of that
	 * sequence.
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 */
	public static void clip(Sequence[] sequences, String[] splitStr) {

		if (splitStr.length == 4) {
			// parse following string to integer
			int position = Integer.parseInt(splitStr[1]);

			// parse following string to integer
			int start = Integer.parseInt(splitStr[2]);

			// parse following string to integer
			int end = Integer.parseInt(splitStr[3]);

			// print out message when the clip index is invalid
			if (start < 0 || start >= sequences[position].getBases().length()) {
				System.out.println("Invalid clip indexes passed");
			}
			// print out message when the stop is more than the length of list
			else if (end > sequences[position].getBases().length()) {
				System.out.println("stop cannot be greater than list length.");
			}
			// print out message when there is no sequence to clip at a specific
			// position
			else if (sequences[position].getBases() == null) {
				System.out.println("There is no sequence to clip at position: " + position);
			} else {

				// use a for loop and removeFirst to clip the front bases
				for (int i = 0; i < start; i++) {
					sequences[position].getBases().removeFirst();

				}

				// use a for loop and removeFirst to clip the bases from the
				// back
				for (int j = sequences[position].getBases().length() + 1; j >= end; j--) {

					sequences[position].getBases().removeLast();
				}

				//
				if (end < start) {
					sequences[position].getBases().clear();
				}

			}

		}

		else if (splitStr.length == 3) {
			// parse following string to integer
			int position = Integer.parseInt(splitStr[1]);

			// parse following string to integer
			int start = Integer.parseInt(splitStr[2]);

			if (start < 0 || start > sequences[position].getBases().length()) {
				System.out.println("Invalid clip indexes passed");
			} else if (sequences[position].getBases().isEmpty()) {
				System.out.println("There is no sequence to clip at position: " + position);
			} else {

				for (int i = 0; i < start; i++) {
					sequences[position].getBases().removeFirst();

				}
			}
		}

	}

	/**
	 * Copy the sequence in position pos1 to pos2
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 */

	@SuppressWarnings("unchecked")
	public static void copy(Sequence[] sequences, String[] splitStr, String[] args) {

		if (splitStr.length == 3) {
			// parse following string to integer and store it in position 1
			int position1 = Integer.parseInt(splitStr[1]);
			// parse following string to integer and store it in position 2
			int position2 = Integer.parseInt(splitStr[2]);

			// print out message if the base is empty
			if (sequences[position1].getBases().isEmpty()) {
				System.out.println("There is no sequence to copy at position: " + position1);
			}

			else {
				// create a new empty sSLList for bases
				SLList<String> bases = new SLList<String>();

				// create a new sequence instance which contains same type with
				// the position one and have an empty linked list
				Sequence sequence2 = new Sequence(sequences[position1].getType(), bases);

				// use a for loop to add the bases to the new SLList
				// this can achieve deep copy (copy by value)
				for (int i = 0; i < sequences[position1].getBases().length(); i++) {
					sequence2.getBases().add(sequences[position1].getBases().getValue(i));
				}

				// it the sequence at position 2 is not empty
				if (sequences[position2] != null) {
					// have to clear it so that we can store the bases
					sequences[position2].getBases().clear();
				}

				// update the sequence at position 2 to sequence
				sequences[position2] = sequence2;

			}

		}
	}

	/**
	 * Swap the tails of the sequences at position1 and position2.
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static void swap(Sequence[] sequences, String[] splitStr) {

		// if the number of argument is 5
		if (splitStr.length == 5) {

			// parse argument 1 into Integer and store it in
			// position1
			int position1 = Integer.parseInt(splitStr[1]);
			// parse argument 2 into Integer and store it in start1
			int start1 = Integer.parseInt(splitStr[2]);
			// parse argument 3 into Integer and store it in
			// position 2
			int position2 = Integer.parseInt(splitStr[3]);
			// parse argument 1 into Integer and store it in start2
			int start2 = Integer.parseInt(splitStr[4]);

			// print error message if the value of the start
			// position is greater than the length of the sequence
			// or less than zero
			if (start1 > sequences[position1].getBases().length() || start1 < 0
					|| start2 > sequences[position2].getBases().length() || start2 < 0) {
				System.out.println("Start position of swap is not valid.");
			}
			// print error message if one of the slots does not
			// contain a sequence
			else if (sequences[position1].getBases().isEmpty()
					|| sequences[position1].getBases().toString2().trim().equals("")) {
				System.out.println("There is no sequences to swap at positions: " + position1);
			}
			// print error message if one of the slots does not
			// contain a sequence
			else if (sequences[position2].getBases().isEmpty()
					|| sequences[position2].getBases().toString2().trim().equals("")) {
				System.out.println("There is no sequences to swap at positions: " + position2);

			}
			// print error message if two sequences are not of the
			// same type
			else if (!(sequences[position1].getType().equals(sequences[position2].getType()))) {
				System.out.println("Have to be same type to swap");
			}

			// else if the length of a sequence is n, the start
			// position may be n, meaning that the tail of the other
			// sequence is appended
			// (i.e., a tail of null length is being swapped).
			else if (start1 == sequences[position1].getBases().length()) {

				// create a SLList StringListToBeInserted
				@SuppressWarnings("unused")
				SLList<String> StringListToBeInserted = new SLList<String>();

				// initialize geneCount to start2
				int geneCount = start2;

				// while geneCount is less than the length of
				// sequence array
				while (geneCount < sequences[position2].getBases().length()) {

					sequences[position1].getBases().add(sequences[position2].getBases().getValue(geneCount));
					// increment geneCount
					geneCount++;

				}

				// set the next of the tail to be null
				sequences[position2].getBases().setTailNextToNull(start2);

			}

			else if (start2 == sequences[position2].getBases().length()) {

				// create a SLList StringListToBeInserted
				@SuppressWarnings("unused")
				SLList<String> StringListToBeInserted = new SLList<String>();

				// initialize geneCount to start1
				int geneCount = start1;

				// while geneCount is less than the length of
				// sequence array
				while (geneCount < sequences[position1].getBases().length()) {

					sequences[position2].getBases().add(sequences[position1].getBases().getValue(geneCount));

					// increment geneCount
					geneCount++;

				}

				sequences[position1].getBases().setTailNextToNull(start1);

			}

		}
	}

	/**
	 * Determine the position of maximum overlap between the sequences at
	 * positions pos1 and pos2.
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 */
	@SuppressWarnings("unused")
	public static void overlap(Sequence[] sequences, String[] splitStr) {

		// parse argument 1 into Integer and store it in
		// position1
		int position1 = Integer.parseInt(splitStr[1]);
		// parse argument 2 into Integer and store it in
		// position2
		int position2 = Integer.parseInt(splitStr[2]);

		// initialize overlap as an empty string
		String overlap = "";

		// initialize max to zero
		int max = 0;

		// set the following booleans to false
		// (they are act as FLAGS!)
		boolean notSameType = false;
		boolean noOverlap = false;
		boolean gotOverlap = false;
		boolean onlyOne = false;

		// initialize index to zero
		int index = 0;

		// if the size is equal to 1
		if (sequences[position1].getBases().getSize() == 1) {

			// use a for loop
			for (int j = 0; j < sequences[position1].getBases().length(); j++) {

				// if it is not a same type, BREAK
				if (notSameType) {
					break;
				}

				// use a for loop start at max
				for (int k = max; k <= j; k++) {

					if (sequences[position1].getBases().getValue(j)
							.equals(sequences[position2].getBases().getValue(k))) {

						// increment max
						max++;
						// do concatenation to get the overlap strings
						overlap += sequences[position1].getBases().getValue(j);
						// index is the difference between j and k
						index = j - k;
						// set the boolean onlyOne to true (only one string
						// overlaps)
						onlyOne = true;

						break;
					}
				}
			}
		}

		// use a for loop
		for (int j = 0; j < sequences[position1].getBases().length(); j++) {

			// if it not a same type
			if (notSameType) {
				break;
			}
			// use a for loop starting at max
			for (int k = max; k < j; k++) {

				// print out message when the types are not the same
				if (!(sequences[position1].getType().equals(sequences[position2].getType())) && !notSameType) {
					System.out.println("Sequences must be of the same type for overlap method.");
					// then set the boolean of notSameType to true
					notSameType = true;
					break;

				}

				// if the types are the same
				else if (sequences[position1].getBases().getValue(j).equals(sequences[position2].getBases().getValue(k))
						&& !notSameType) {

					// increment max
					max++;
					// do concatenation to get the overlap strings
					overlap += sequences[position1].getBases().getValue(j);
					// index is the difference between j and k
					index = j - k;
					// set the gotOverlap to true means there is an overlap
					gotOverlap = true;
					break;
				} else {
					// then set the noOverlap to true if there is no overlap
					noOverlap = true;
					break;
				}

			}

		}

		// if there is only one
		if (onlyOne) {
			System.out.println("Overlap starts at pos " + index + " ; bases that overlap " + overlap);
			onlyOne = false;
		}
		// if there is an overlap
		else if (gotOverlap) {
			System.out.println("Overlap starts at pos " + index + " ; bases that overlap " + overlap);
			gotOverlap = false;
		}
		// if there is no overlap
		else if (noOverlap) {
			System.out.println("There is no overlap in these sequences.");
			noOverlap = false;
		}

	}

	/**
	 * Transcription converts a DNA sequence to an RNA sequence at pos1.
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static void transcribe(Sequence[] sequences, String[] splitStr) {
		if (splitStr.length == 2) {
			// parse argument 1 into Integer and store it in
			// position
			int position = Integer.parseInt(splitStr[1]);

			// check if the bases is empty then print out a message
			if (sequences[position].getBases().isEmpty()) {
				System.out.println("No sequence to be transcribed");

			}
			// if the type is DNA
			else if (sequences[position].getType().equals("DNA")) {
				// then have to set it to RNA before transcribing
				sequences[position].setType("RNA");

				// call the method transcribeProcess in the Sequence class
				sequences[position].transcribeProcess(sequences[position].getBases());

				// reverse the bases
				sequences[position].getBases().reverse();

			}
			// print out message when the type is RNA
			else if (sequences[position].getType().equals("RNA")) {
				System.out.println("Sequence type must be DNA.");
			}
			// print out message when the type is empty
			else if (sequences[position].getType().isEmpty()) {
				System.out.println("Sequence type must be DNA.");
			}
		}
	}

	/**
	 * Translation converts an RNA sequence in to an amino acid sequence at
	 * pos1.
	 * 
	 * @param sequences
	 *            the array sequence
	 * 
	 * @param spliStr
	 *            array that stores the split command
	 * 
	 */
	public static void translate(Sequence[] sequences, String splitStr[]) {
		// since the translation converts groups of three RNA bases into their
		// equivalent amino acid

		// parse argument 1 into Integer and store it in
		// position
		int position = Integer.parseInt(splitStr[1]);

		// initialize the length of bases
		int lengthOfBases = sequences[position].getBases().length();

		// check if the specified sequence is empty
		if (sequences[position] == null) {
			System.out.println("There is no sequence at position:" + position);

		}

		// check if the type is not RNA
		else if (!(sequences[position].getType().equals("RNA"))) {
			System.out.println("Type must be RNA");

		}

		else {
			// we have to check whether there is a start codon (AUG) present, so
			// we need a boolean ( acting as a flag) to determine whether there
			// is a start codon or not
			boolean hasStart = false;

			// initialize endLength
			int endLength = sequences[position].getBases().length();

			// initialize the first start index as zero
			int startIndex = 0;

			// use a for loop to check for the start codon
			for (int i = 0; i < endLength; i++) {

				// if the bases contain AUG
				// i - first index
				// i + 1 - second index
				// i + 2 - third index
				if ((sequences[position].getBases().getValue(i).equals("A"))
						&& ((i + 1) < endLength && sequences[position].getBases().getValue(i + 1).equals("U"))
						&& ((i + 2) < endLength && sequences[position].getBases().getValue(i + 2).equals("G"))) {

					// set the macthStart boolean to true
					hasStart = true;

					// reassign the startIndex to i
					startIndex = i;

					// break
					break;
				}
			}

			// if we can't find any AUG, then tell users that tehre is no start
			// codon AUG
			if (!hasStart) {
				System.out.println("There is no start codon");
			}

			// create a boolean hasStop and set it to false ( which also acts
			// as another flag)
			boolean hasStop = false;
			// initialize the matachEndIndex to zero
			int hasStopIndex = 0;

			// use a for loop to find out whetehr there is any stop codon (UAA,
			// UAG, UGA)
			for (int i = startIndex + 3; i < endLength; i += 3) {
				if (((sequences[position].getBases().getValue(i).equals("U"))
						&& (sequences[position].getBases().getValue(i + 1).equals("A"))
						&& (sequences[position].getBases().getValue(i + 2).equals("A")))
						|| ((sequences[position].getBases().getValue(i).equals("U"))
								&& (sequences[position].getBases().getValue(i + 1).equals("A"))
								&& (sequences[position].getBases().getValue(i + 2).equals("G"))
								|| ((sequences[position].getBases().getValue(i).equals("U"))
										&& (sequences[position].getBases().getValue(i + 1).equals("G"))
										&& (sequences[position].getBases().getValue(i + 2).equals("A"))))) {

					// if they found it, then set the boolean hasStop to true
					hasStop = true;
					// reassign the match index to i
					hasStopIndex = i;
					// break
					break;
				}
			}
			if (!hasStop) {
				System.out.println("There is no end codon");

			}

			// create a SLList to store the aminoacid
			SLList<String> aminoAcid = new SLList<String>();
			// set the type of aminoacid to AA
			Sequence aminoAcidSequence = new Sequence("AA", aminoAcid);
			
			
			for (int i = startIndex; i < hasStopIndex; i+=3) {
				
				// UUU - Phenylalanine
				if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("F");
				}

				// UUC - Phenylalanine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("F");
				}

				// UUA - Leucine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("L");
				}

				// UUG - Leucine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("L");
				}

				// CUU - Leucine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("L");
				}

				// CUC - Leucine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("L");
				}

				// CUA - Leucine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("L");
				}

				// CUG - Leucine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("L");
				}

				// AUU - Isoleucine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("I");
				}

				// AUC - Isoleucine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("I");
				}

				// AUA - Isoleucine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("I");
				}

				// AUG - Isoleucine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("M");
				}

				// GUU - Valine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("V");
				}

				// GUC - Valine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("V");
				}

				// GUA - Valine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("V");
				}

				// GUG - Valine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("U") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("V");
				}

				// UCU - Serine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("S");
				}

				// UCC - Serine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("S");
				}

				// UCA - Serine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("S");
				}

				// UCG - Serine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("S");
				}

				// CCU - Proline
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("P");
				}

				// CCC - Proline
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("P");
				}

				// CCA - Proline
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("P");
				}

				// CCG - Proline
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("P");
				}

				// ACU - Threonine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("T");
				}

				// ACC - Threonine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("T");
				}

				// ACA - Threonine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("T");
				}

				// ACG - Threonine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("T");
				}

				// GCU - Alanine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("A");
				}

				// GCC - Alanine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("A");
				}

				// GCA - Alanine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("A");
				}

				// GCG - Alanine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("C") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("A");
				}

				// UAU - Tyrosine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("Y");
				}

				// UAC - Tyrosine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("Y");
				}

				// UAA - Tyrosine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					// i = i + 3;
				}

				// UAG - Tyrosine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					// i = i + 3;
				}

				// CAU - Histidine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("H");
				}

				// CAC - Histidine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("H");
				}

				// CAA -Glutamine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("Q");
				}

				// CAG -Glutamine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("Q");
				}

				// AAU - Asparagine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("N");
				}

				// AAC - Asparagine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("N");
				}

				// AAA - Lysine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("K");
				}

				// AAG - Lysine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("K");
				}

				// GAU - Aspartaic acid
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("D");
				}

				// GAC - Aspartaic acid
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("D");
				}

				// GAA - Glutamic acid
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("E");
				}

				// GAG - Glutamic acid
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("A") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("E");
				}

				// UGU - Cysteine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("C");
				}

				// UGC - Cysteine
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("C");
				}

				// UGG - Trytophan
				else if (sequences[position].getBases().getValue(i).equals("U") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("W");
				}

				//// CGU - Arginine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("R");
				}

				// CGC - Arginine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("R");
				}

				// CGA - Arginine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("R");
				}

				// CGG - Arginine
				else if (sequences[position].getBases().getValue(i).equals("C") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("R");
				}

				// AGU - Serine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("S");
				}

				// AGC - Serine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("S");
				}

				// AGA - Arginine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("R");
				}

				// AGG - Arginine
				else if (sequences[position].getBases().getValue(i).equals("A") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("R");
				}
				// GGU - Glycine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("U")) {
					aminoAcid.add("G");
				}

				// GGC - Glycine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("C")) {
					aminoAcid.add("G");
				}

				// GGA - Glycine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("A")) {
					aminoAcid.add("G");
				}

				// GGC - Glycine
				else if (sequences[position].getBases().getValue(i).equals("G") && (i + 1) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 1).equals("G") && (i + 2) < lengthOfBases
						&& sequences[position].getBases().getValue(i + 2).equals("G")) {
					aminoAcid.add("G");
				} 

			}

			sequences[position].getBases().clear();
			sequences[position] = aminoAcidSequence;
		}

	}
}
