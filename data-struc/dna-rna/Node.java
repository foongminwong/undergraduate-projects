/**
 *   Node: contain a list of methods for nodes.
 */

/**
 * @author Foong Min Wong
 *
 */
public class Node<E> {
	private E element;
	private Node<E> next;
	private Node<E> prev; // reference to the subsequent node in the list

	/**
	 * Creates a new node with a null next field
	 * 
	 * @param item
	 *            The data stored
	 */
	public Node(E item) {
		element = item;
		next = null;
		prev = null;

	}

	public Node(E it, Node<E> p, Node<E> n) {
		element = it;
		prev = p;
		next = n;
	}

	/**
	 * Creates a new node that references another node
	 * 
	 * @param item
	 *            The data stored
	 * @param nodeRef
	 *            The node referenced by new node
	 */
	public Node(E item, Node<E> nodeRef) {
		element = item;
		next = nodeRef;
	}

	public Node() {
		element = null;
		next = null;
	}

	public Node(Node<E> nextval) {
		next = nextval;
	}

	// get and set
	public Node<E> getNext() {
		return next;
	}

	public Node<E> setNext(Node<E> nextval) {
		return next = nextval;
	} // Return element field

	public E getElement() {
		return element;
	} // Set element field

	public E setElement(E item) {
		return element = item;
	}

	public Node<E> setPrev(Node<E> prevval) {
		return prev = prevval;
	}

	public Node<E> getPrev() {
		// TODO Auto-generated method stub
		return prev;
	}

}
