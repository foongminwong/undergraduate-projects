package hw5;

import hw5.speccheck.Note;
import hw5.speccheck.WavIO;

public class Main {

	public static void main(String[] args) {
		//Note[] twinkle = Note.parse("4C4 4C4 4G4 4G4 4A4 4A4 2G4");
		Note[] bnb = Note.parse("4G3 4A#3 4D4 4D#4 2G#3 16D4(0.0) 4G3 4A#3 4D4 4D#4 2F4 16D4(0.0) 4D#4 4F4 4G4 4G#4 2A#4 16D4(0.0) 4A#4 4G#4 4G4 4F4 2D#4 16D4(0.0) 4G#4 4G4 4F4 4D#4 2A#3 8D4(0.0) 4G3 4A#3 4D4 4D#4 2G#3 16D4(0.0) 4G3 4A#3 4D4 4D#4 2F4 16D4(0.0) 4G4 4F4 4G4 4A#4 2D#4 16D4(0.0) 4D#4 4D4 4D#4 4G4 2C4 16D4(0.0) 4G4 4G#4 4F4 4G4 2D#4 8D4(0.0) 4D#4 4F4 4G4 4G#4 2A#4 16D4(0.0) 4D#4 4F4 4G4 4C5 2A#4 16D4(0.0) 4D#4 4F4 4G4 4G#4 2A#4 16D4(0.0) 4A#4 4G#4 4G4 4F4 2D#4 16D4(0.0) 4D#4 4F4 4G4 4D#4 2F4 8D4(0.0) 4A3 4C4 4E4 4F4 2A#3 16D4(0.0) 4A3 4C4 4E4 4F4 2G4 16D4(0.0) 4A4 4G4 4A4 4C5 2F4 16D4(0.0) 4F4 4E4 4F4 4A4 2D4 16D4(0.0) 4A4 4A#4 4G4 4A4 2F4 8D4(0.0)");
		//WavIO.write(twinkle, 10, new String(System.getProperty("user.home") + "/Desktop/twinkle.wav"));
		WavIO.write(bnb, 170, new String(System.getProperty("user.home") + "/Desktop/beauty_and_the_beast.wav"));
	}
}
