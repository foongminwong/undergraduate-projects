package hw5;

import hw5.speccheck.Note;

public class MusicUtilities {
	public static Note[] getScale(Note rootNote, int[] halfstepIDOffsets) {

		int length = halfstepIDOffsets.length;

		Note[] scale = new Note[length + 1]; // bracket

		for (int i = 0; i < length + 1; i++) {

			if (i == 0) {
				scale[0] = rootNote;
			}

			else {
				scale[i] = new Note(scale[i - 1].getHalfstepID() + halfstepIDOffsets[i - 1], rootNote.getDuration(),
						rootNote.getAmplitude(), rootNote.isDotted());
			}
		}

		return scale;

	}

	public static Note[] getMajorScale(Note rootNote) {
		Note[] majorScale = new Note[8];
		int[] offSetsMajor = { 2, 2, 1, 2, 2, 2, 1 };

		for (int i = 0; i < majorScale.length; i++) {

			if (i == 0) {
				majorScale[i] = new Note(rootNote.getHalfstepID(), rootNote.getDuration(), rootNote.getAmplitude(),
						rootNote.isDotted());
			}

			else {
				majorScale[i] = new Note(majorScale[i - 1].getHalfstepID() + offSetsMajor[i - 1],
						rootNote.getDuration(), rootNote.getAmplitude(), rootNote.isDotted());
			}

		}

		return majorScale;
	}

	public static Note[] getMinorPentatonicBluesScale(Note rootNote) {
		Note[] minorScale = new Note[6];
		int[] offSetsMinor = { 3, 2, 2, 3, 2 };

		for (int i = 0; i < minorScale.length; i++) {

			if (i == 0) {
				minorScale[i] = new Note(rootNote.getHalfstepID(), rootNote.getDuration(), rootNote.getAmplitude(),
						rootNote.isDotted());
			}

			else {
				minorScale[i] = new Note(minorScale[i - 1].getHalfstepID() + offSetsMinor[i - 1],
						rootNote.getDuration(), rootNote.getAmplitude(), rootNote.isDotted());
			}

		}

		return minorScale;
	}

	public static Note[] getBluesScale(Note rootNote) {
		Note[] bluesScale = new Note[7];
		int[] offSetsBluesScale = { 3, 2, 1, 1, 3, 2 };

		for (int i = 0; i < bluesScale.length; i++) {

			if (i == 0) {
				bluesScale[i] = new Note(rootNote.getHalfstepID(), rootNote.getDuration(), rootNote.getAmplitude(),
						rootNote.isDotted());
			}

			else {
				bluesScale[i] = new Note(bluesScale[i - 1].getHalfstepID() + offSetsBluesScale[i - 1],
						rootNote.getDuration(), rootNote.getAmplitude(), rootNote.isDotted());
			}

		}
		return bluesScale;
	}

	public static Note[] getNaturalMinorScale(Note rootNote) {
		Note[] naturalMinorScale = new Note[8];
		int[] offSetsNaturalMinor = { 2, 1, 2, 2, 1, 2, 2 };

		for (int i = 0; i < naturalMinorScale.length; i++) {

			if (i == 0) {
				naturalMinorScale[i] = new Note(rootNote.getHalfstepID(), rootNote.getDuration(),
						rootNote.getAmplitude(), rootNote.isDotted());
			}

			else {
				naturalMinorScale[i] = new Note(naturalMinorScale[i - 1].getHalfstepID() + offSetsNaturalMinor[i - 1],
						rootNote.getDuration(), rootNote.getAmplitude(), rootNote.isDotted());
			}

		}
		return naturalMinorScale;
	}

	public static Note[] join(Note firstNotes[], Note secondNotes[]) {

		int lengthOfFirstNotes = firstNotes.length;
		int lengthOfSecondNotes = secondNotes.length;
		int sumOfLength = lengthOfFirstNotes + lengthOfSecondNotes;

		Note[] newArray = new Note[sumOfLength];

		for (int i = 0; i < sumOfLength; i++) {

			if (i < lengthOfFirstNotes) {
				newArray[i] = firstNotes[i];
			}

			else {
				newArray[i] = secondNotes[i - lengthOfFirstNotes];
			}
		}
		return newArray;
	}

	public static Note[] repeat(Note notes[], int cycle) {

		int notesLength = notes.length;
		Note[] repeatedArrays = new Note[notesLength * cycle];

		for (int i = 0; i < repeatedArrays.length; i++) {
			repeatedArrays[i] = notes[i % notes.length];
			// use modulus to get the elements again ;D
		}
		return repeatedArrays;
	}

	public static Note[] ramplify(Note notes[], double startingAmplitude, double endingAmplitude) {

		Note[] ramplifiedNotes = new Note[notes.length];
		double differenceAmplitude = endingAmplitude - startingAmplitude;
		double distanceBetweenEachAmplitude = (double) differenceAmplitude / (ramplifiedNotes.length - 1);

		for (int i = 0; i < ramplifiedNotes.length; i++) {
			if (i == 0) {
				ramplifiedNotes[i] = new Note(notes[i].getHalfstepID(), notes[i].getDuration(), startingAmplitude,
						notes[i].isDotted());
			}

			else if (i == (ramplifiedNotes.length - 1)) {
				ramplifiedNotes[i] = new Note(notes[i].getHalfstepID(), notes[i].getDuration(), endingAmplitude,
						notes[i].isDotted());
			}

			else {
				ramplifiedNotes[i] = new Note(notes[i].getHalfstepID(), notes[i].getDuration(),
						ramplifiedNotes[i - 1].getAmplitude() + distanceBetweenEachAmplitude, notes[i].isDotted());
			}

		}

		return ramplifiedNotes;
	}

	public static Note[] reverse(Note notes[]) {

		Note[] reverseNotes = new Note[notes.length];

		// copy array
		for (int i = 0; i < notes.length; i++) {
			reverseNotes[i] = notes[i];
		}

		for (int i = 0; i < reverseNotes.length / 2; i++) {
			Note temp = reverseNotes[i];
			reverseNotes[i] = reverseNotes[reverseNotes.length - i - 1];
			// the last one
			reverseNotes[reverseNotes.length - i - 1] = temp;
		}

		return reverseNotes;
	}

	public static Note[] transpose(Note notes[], Note rootNote) {

		Note[] transposeNotes = new Note[notes.length];

		int amountOfShift = notes[0].getHalfstepID() - rootNote.getHalfstepID();

		for (int i = 0; i < transposeNotes.length; i++) {
			if (i == 0) {
				transposeNotes[i] = new Note(rootNote.getHalfstepID(), notes[i].getDuration(), notes[i].getAmplitude(),
						notes[i].isDotted());
			}

			else {
				transposeNotes[i] = new Note(notes[i].getHalfstepID() - amountOfShift, notes[i].getDuration(),
						notes[i].getAmplitude(), notes[i].isDotted());
			}
		}
		return transposeNotes;
	}

	public static Note[] invert(Note notes[], Note pivotNote) {

		Note[] inversedNotes = new Note[notes.length];

		for (int i = 0; i < inversedNotes.length; i++) {

			int amountOfShift = (notes[i].getHalfstepID() - pivotNote.getHalfstepID()) * 2;

			if (notes[i] == pivotNote) {
				inversedNotes[i] = new Note(notes[i].getHalfstepID(), notes[i].getDuration(), notes[i].getAmplitude(),
						notes[i].isDotted());
			}

			else {
				inversedNotes[i] = new Note(notes[i].getHalfstepID() - amountOfShift, notes[i].getDuration(),
						notes[i].getAmplitude(), notes[i].isDotted());
			}
		}

		return inversedNotes;
	}
}
