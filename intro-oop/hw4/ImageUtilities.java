package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageUtilities {

	public static BufferedImage swapCorners(BufferedImage image) {

		BufferedImage swappedImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());

		int originalHeight = image.getHeight();
		int originalWidth = image.getWidth();
		int halfHeightOfImage = image.getHeight() / 2;
		int halfWidthOfImage = image.getWidth() / 2;

		int xPixel = 0, yPixel = 0, rgb = 0;

		// row = height, column = width
		for (int y = 0; y < originalHeight; y++) {
			for (int x = 0; x < originalWidth; x++) {
				xPixel = (halfWidthOfImage + x) % originalWidth;
				yPixel = (halfHeightOfImage + y) % originalHeight;
				rgb = image.getRGB(xPixel, yPixel);
				swappedImage.setRGB(x, y, rgb);
			}

		}

		return swappedImage;
	}

	public static BufferedImage getCircleMask(int width, int height, double power) {
		BufferedImage grayImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);

		// find c and r pixels distance from image center using Pythagorean
		// theorem
		double midC = width / 2.0;
		double midR = height / 2.0;

		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				// System.out.println(c + " " + r);
				// circle equation
				double distancefromCenter = Math.sqrt(Math.pow(c - midC, 2) + Math.pow(r - midR, 2));
				double radius = Math.min(midC, midR);
				double normalizedDistance = distancefromCenter / radius;
				// cannot be larger than radius

				if (distancefromCenter >= radius) {
					// border or outside circle
					grayImage.setRGB(c, r, new Color(0, 0, 0).getRGB());
					// set as black
				}

				else {
					// inside the circle
					double tweakedDistance = Math.pow(normalizedDistance, power); // why?
					float rgbGray = (float) (1 - tweakedDistance);
					// gray = 1 - tweakedDistance which will be around [0,1]
					// int rgbGray = (int) ((1 - tweakedDistance) * 255);
					grayImage.setRGB(c, r, new Color(rgbGray, rgbGray, rgbGray).getRGB());
					// set as grayscale/gradient
				}
			}
		}
		return grayImage;
	}

	public static int mix(int a, int b, double weight) {
		int weightedBlend = (int) (weight * a + (1 - weight) * b);
		return weightedBlend;
	}

	public static Color mix(Color a, Color b, double weight) {

		int aRed = a.getRed();
		int aGreen = a.getGreen();
		int aBlue = a.getBlue();

		int bRed = b.getRed();
		int bGreen = b.getGreen();
		int bBlue = b.getBlue();

		int newRed = mix(aRed, bRed, weight);
		int newGreen = mix(aGreen, bGreen, weight);
		int newBlue = mix(aBlue, bBlue, weight);

		Color ab = new Color(newRed, newGreen, newBlue);

		return ab;
	}

	public static BufferedImage addMasked(BufferedImage imageA, BufferedImage grayscaleMaskA, BufferedImage imageB,
			BufferedImage grayscaleMaskB) {

		BufferedImage blendedImage = new BufferedImage(imageA.getWidth(), imageA.getHeight(), imageA.getType());

		for (int row = 0; row < imageA.getHeight(); row++) {
			for (int column = 0; column < imageA.getWidth(); column++) {

				Color colorA = new Color(grayscaleMaskA.getRGB(column, row));
				int weightA = colorA.getRed();

				Color colorB = new Color(grayscaleMaskB.getRGB(column, row));
				int weightB = colorB.getRed();

				int weightSum = weightA + weightB;

				if (weightSum == 0) {
					Color rgbForBlendedImage = mix(new Color(imageA.getRGB(column, row)),
							new Color(imageB.getRGB(column, row)), 0.5);
					blendedImage.setRGB(column, row, rgbForBlendedImage.getRGB());
				}

				else {
					double weight = (double) weightA / weightSum;
					// weight proportion
					Color rgbForBlendedImage = mix(new Color(imageA.getRGB(column, row)),
							new Color(imageB.getRGB(column, row)), weight);
					blendedImage.setRGB(column, row, rgbForBlendedImage.getRGB());
				}
			}
		}

		return blendedImage;
	}

	public static BufferedImage makeSeamless(BufferedImage imageToBeSeamless, double power) {

		BufferedImage imageB = swapCorners(imageToBeSeamless);
		BufferedImage maskA = getCircleMask(imageToBeSeamless.getWidth(), imageToBeSeamless.getHeight(), power);
		BufferedImage maskB = swapCorners(maskA);
		BufferedImage mix = addMasked(imageToBeSeamless, maskA, imageB, maskB);

		return mix;
	}

	public static BufferedImage tile(BufferedImage imageToTile, int numberOfHorizontalRepetition,
			int numberOfVerticalRepetition) {

		int tileHorizontal = imageToTile.getWidth() * numberOfHorizontalRepetition;
		int tileVertical = imageToTile.getHeight() * numberOfVerticalRepetition;

		int incrementR = (imageToTile.getHeight() * numberOfVerticalRepetition) / numberOfVerticalRepetition;
		int incrementC = (imageToTile.getWidth() * numberOfHorizontalRepetition) / numberOfHorizontalRepetition;

		BufferedImage newImage = new BufferedImage(tileHorizontal, tileVertical, imageToTile.getType());

		for (int r = 0; r < imageToTile.getHeight(); r++) {
			for (int c = 0; c < imageToTile.getWidth(); c++) {
				int rgb = imageToTile.getRGB(c, r);
				newImage.setRGB(c, r, rgb);

				for (int i = 0; i < numberOfHorizontalRepetition; i++) {
					for (int j = 0; j < numberOfVerticalRepetition; j++) {

						newImage.setRGB(c + i * incrementC, r + j * incrementR, rgb);
					}

				}

			}
		}

		return newImage;
	}
}
