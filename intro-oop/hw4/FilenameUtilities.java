package hw4;

import java.io.File;

public class FilenameUtilities {

	public static String getExtension(File file) {
		String fileName = file.getName();

		int lastPeriod = fileName.lastIndexOf('.');
		int firstPeriod = fileName.indexOf(".");

		if (fileName.startsWith(".") && firstPeriod != lastPeriod) {
			return fileName.substring(lastPeriod + 1); // e.g: ../hidden.congfig
		} else if (fileName.contains(".") && !(fileName.startsWith("."))) {
			return fileName.substring(lastPeriod + 1);
		} else
			return null;
	}

	public static File appendToName(File file, String textToBeAppendWithFile) {
		File anotherFile = new File(file.getParentFile(), file.getName());
		
		String fileName = anotherFile.toString();

		String text = "_" + textToBeAppendWithFile;
 
		int lastPeriod = fileName.lastIndexOf(".");
		 
		if (FilenameUtilities.getExtension(anotherFile) == null) {
			return new File(fileName + text);
		} 

		else {
			int indexOfExtension = fileName.indexOf(FilenameUtilities.getExtension(anotherFile));
			return new File(fileName.substring(0,lastPeriod) + text + fileName.substring(lastPeriod));
		}

	}
}
