package hw4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

public class Main {

	public static void main(String[] args) throws IOException {
		JFileChooser dialog = new JFileChooser();
		int status = dialog.showOpenDialog(null);
	  //BufferedImage img = new BufferedImage(1024,1024,BufferedImage.TYPE_BYTE_GRAY);
		if (status == JFileChooser.APPROVE_OPTION) {
			File file = dialog.getSelectedFile();
			BufferedImage src = ImageIO.read(file);
			
			BufferedImage dst = ImageUtilities.swapCorners(src);
			BufferedImage dst1 = ImageUtilities.getCircleMask(src.getWidth(),src.getHeight(),5);
			BufferedImage dst2 = ImageUtilities.swapCorners(dst1);
			BufferedImage dst3 = ImageUtilities.makeSeamless(src, 3);
			BufferedImage tile = ImageUtilities.tile(src, 4, 3);
			BufferedImage tile2 = ImageUtilities.tile(dst3, 4, 3);
			
			ImageIO.write(dst, FilenameUtilities.getExtension(file), FilenameUtilities.appendToName(file, "swapped") );
			System.out.println(FilenameUtilities.appendToName(file, "swapped"));
			File files = new File("dir.june/ai17");
			System.out.println(FilenameUtilities.appendToName(file, "swapped"));
			System.out.println(FilenameUtilities.appendToName(files, "_backup"));
			
			ImageIO.write(dst, "png", new File(new File(System.getProperty("user.home"), "Desktop"), "out.png"));
			ImageIO.write(dst1, "png", new File(new File(System.getProperty("user.home"), "Desktop"), "out1.png"));
			ImageIO.write(dst2, "png", new File(new File(System.getProperty("user.home"), "Desktop"), "out2.png"));
			ImageIO.write(dst3, "png", new File(new File(System.getProperty("user.home"), "Desktop"), "out3.png"));
			ImageIO.write(tile, "png", new File(new File(System.getProperty("user.home"), "Desktop"), "mrbean.png"));
			ImageIO.write(tile2, "png", new File(new File(System.getProperty("user.home"), "Desktop"), "mrbean2.png"));
			
			
		}
	}
}
