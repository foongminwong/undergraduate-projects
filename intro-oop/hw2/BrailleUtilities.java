/Created by FoongMinWong
package hw2;

import java.util.Scanner;

public class BrailleUtilities {

	public static final String RAISED = "\u2022"; // the huge dot
	public static final String UNRAISED = "\u00b7";// the small dot
	public static final String LETTER_SPACER = "  ";
	public static final String WORD_SPACER = "    ";

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.print("Enter a text you want it to be translated: ");
		String textToBeTranslated = in.nextLine();
		System.out.print(translate(textToBeTranslated));
	}

	public static String translateTopLine(String textToBeTranslated) {

		// replace space to WORD_SPACER
		textToBeTranslated = textToBeTranslated.replaceAll(" ", WORD_SPACER);
		
		//lowercase the letters
		textToBeTranslated = textToBeTranslated.toLowerCase();

		//repalce each letter to corresponding DOTS
		textToBeTranslated = textToBeTranslated.replaceAll("[a,b,e,h,k,l,o,r,u,v,z]",
				(RAISED + UNRAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[c,d,f,g,m,n,p,q,x,y]", (RAISED + RAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[i,j,s,t,w]", (UNRAISED + RAISED + LETTER_SPACER));

		return textToBeTranslated.trim();
	}

	public static String translateMiddleLine(String textToBeTranslated) {

		textToBeTranslated = textToBeTranslated.replace(" ", WORD_SPACER);
		textToBeTranslated = textToBeTranslated.toLowerCase();
		

		textToBeTranslated = textToBeTranslated.replaceAll("[b,f,i,l,p,s,v]", (RAISED + UNRAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[g,h,j,q,r,t,w]", (RAISED + RAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[d,e,n,o,y,z]", (UNRAISED + RAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[a,c,k,m,u,x]", (UNRAISED + UNRAISED + LETTER_SPACER));
		
		return textToBeTranslated.trim();
	}

	public static String translateBottomLine(String textToBeTranslated) {

		textToBeTranslated = textToBeTranslated.replace(" ", WORD_SPACER);
		textToBeTranslated = textToBeTranslated.toLowerCase();

		textToBeTranslated = textToBeTranslated.replaceAll("[k,l,m,n,o,p,q,r,s,t]",
				(RAISED + UNRAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[u,v,x,y,z]", (RAISED + RAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[w]", (UNRAISED + RAISED + LETTER_SPACER));
		textToBeTranslated = textToBeTranslated.replaceAll("[a,b,c,d,e,f,g,h,i,j]",
				(UNRAISED + UNRAISED + LETTER_SPACER));

		return textToBeTranslated.trim();
	}

	public static String translate(String textToBeTranslated) {

		String top = translateTopLine(textToBeTranslated);
		String middle = translateMiddleLine(textToBeTranslated);
		String bottom = translateBottomLine(textToBeTranslated);

		//%s = string ; %n = newline
		textToBeTranslated = String.format("%s%n%s%n%s%n", top, middle, bottom);
		System.out.println();
		return textToBeTranslated;// don't trim, you will lose the new line after that
	}
}

//textToBeTranslated = top + "\n" + middle + "\n" + bottom; << DON'T USE \n to separate lines >>
