// Label.h
/* Header for Label (Label.h) */
#ifndef LABEL_H
#define LABEL_H
#include "Widget.h"
class Label : public Widget {

protected:
int x, y;   // xcoord of label's 1st charac, label's ycoord
string text; //label text

public:
Label(int x, int y, const string & text);       // Constructor

//extend Rectangle
//int GetLeft() const;
//int GetRight() const;
//int GetTop() const;
//int GetBottom() const;
//bool Contains(int xc, int yc) const;

void Draw();
void SetText(const string & text);

};

#endif
