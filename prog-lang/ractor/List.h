// List.h
/* Header for List (List.h) */
#ifndef LIST_H
#define LIST_H

//#include <functional>
#include "Widget.h"

//#include <vector>
//#include <utility>

class List : public Widget {

protected:
int x, y; // private data memebers
vector<string> stringVector;
vector<function<void(int)> > listeners;
int count = 0;
bool same = false;
bool selected = false;
struct Coordinate
{
        int xx,yy;
        Coordinate(int x, int y) : xx(x),yy(y){
        }
};

vector<Coordinate> coords;

int selectedY = 0;

string text = "";

public:
List(int x, int y, const vector<string> & stringVector); // Constructor


int GetSelectedIndex() const;
const string & GetSelected() const;



void Draw();
void AddListener(std::function<void(int)> listener);
void OnMouseClick(int c1, int c2);

};

#endif
