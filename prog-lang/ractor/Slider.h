// Slider.h

/* Header for Slider (Slider.h) */
#ifndef SLIDER_H
#define SLIDER_H

#include <functional>
#include "Widget.h"

class Slider : public Widget {

protected:
int x, y, sliderMax,sliderDefault;
std::vector<function<void(int)> > listeners;

public:
Slider(int x, int y, int sliderMax, int sliderDefault); // Constructor

int GetValue() const;
int GetMax() const;
void Draw();
void AddListener(std::function<void(int)> listener);
void OnMouseClick(int c1, int c2);

};

#endif
