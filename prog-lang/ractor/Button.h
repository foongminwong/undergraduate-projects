// Button.h

/* Header for Button (Button.h) */
#ifndef BUTTON_H
#define BUTTON_H

#include "Widget.h"

class Button : public Widget {

protected:
int x, y;   // private data memebers
string text;
//int m_count,x_count,y_count;
std::vector<function<void()> > listeners;
//bool selected = false;
public:
Button(int x, int y, const string & text);       // Constructor

//extend Widget
void Draw();
void AddListener(std::function<void()> l);
void OnMouseClick(int x, int y);

};

#endif
