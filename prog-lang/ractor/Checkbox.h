// Checkbox.h

/* Header for Checkbox (Checkbox.h) */
#ifndef CHECKBOX_H
#define CHECKBOX_H
#include "Widget.h"

class Checkbox : public Widget {

protected:
int x, y; // private data memebers
string text;
bool state = false;
bool isChecked;
std::vector<function<void(bool)> > listeners;

public:
Checkbox(int x, int y, const string &text); // Constructor

bool IsChecked();
void IsChecked(bool state);


void Draw();
void AddListener(std::function<void(bool)> listener);
void OnMouseClick(int c1, int c2);

};

#endif
