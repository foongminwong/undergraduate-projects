// Widget.cpp
#include "Widget.h"

Widget::Widget(int l, int r, int t, int b):Rectangle(l,r,t,b){
this->l = l;
this->r = r;
this-> t = t;
this->b = b;

}

Widget::~Widget(){
//delete this;
}

// inherited from superclass Rectangle
int Widget::GetLeft() const {
        return l;
}

int Widget::GetRight() const {
        return r;
}

int Widget::GetTop() const {
        return t;
}

int Widget::GetBottom() const {
        return b;
}

bool Widget::Contains(int xc, int yc) const {
        return (xc >= this->GetLeft() && xc <= this->GetRight() && yc >= this->GetTop() && yc <= this->GetBottom());
}

// no drawing
void Widget::Draw(){
  //      cerr << "default widget draw" << endl;
}

// no clicking
void Widget::OnMouseClick(int x, int y){
//        cerr << "default widget onmouseclick" << endl;
}
