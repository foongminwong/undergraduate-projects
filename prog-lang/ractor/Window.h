//Window.h
/* Header for Window (Window.h) */
#ifndef WINDOW_H
#define WINDOW_H

#include "Widget.h"

class Window : public Widget {

protected:
int l,r,t,b;   // private data memebers
vector<Widget*> widgets;
bool stop;

public:
Window();   // Constructor

~Window();

void Add(Widget* w);
void Draw();   // clear
void Loop();
void Stop();

};

#endif
