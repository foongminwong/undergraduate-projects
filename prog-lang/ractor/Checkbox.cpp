// Checkbox.cpp

#include "Checkbox.h"

Checkbox::Checkbox(int x, int y, const string & text):Widget(x,x+text.length()+3,y,y){
//        cerr << "initialize checkbox" << endl;
        this->x = x;
        this->y = y;
        this->text = text;
}

bool Checkbox::IsChecked(){
  //      cerr << "bool checkbox ischecked " << this->state << endl;
        return this->state;
}

//set the current state
void Checkbox::IsChecked(bool b){
        this->state = b;
    //    cerr << "void ischeck" << b << "then" << this->state;
}
void Checkbox::Draw(){
        string bracketwx = "[x]";
        string bracket = "[ ]";
        
	//if(IsChecked()){
	if(state == true){
                mvprintw(this->y,this->x,  (bracketwx + text).c_str());
	}
        else
                mvprintw(this->y,this->x,  (bracket + text).c_str());
}



void Checkbox::AddListener(function<void(bool)> l){
      //  cerr << "checkbox addlistener" << endl;
        listeners.push_back(l);
}

void Checkbox::OnMouseClick(int c1, int c2){
       // cerr << "checkbox mouseclick" << endl;
	//isCheck
	state = !state;
        //state = true;
	
	//IsChecked(1);
        for (unsigned int i = 0; i < listeners.size(); i++) {
                listeners[i](state);
        }
	//state = false;
}
