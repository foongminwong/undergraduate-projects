// Window.cpp
#include "Window.h"

Window::Window():Widget(l,r,t,b){
        initscr();
        noecho();
        curs_set(0); // disable cursor
        keypad(stdscr, TRUE);
        mousemask(BUTTON1_CLICKED, NULL);
        
}
Window::~Window(){
        for(unsigned int i=0; i<widgets.size(); i++) {
                delete widgets[i];
        }
}

void Window::Add(Widget* w){
        //mvprintw(w->x,w->y,w->text);
        //cerr << "Window Add" << endl;
        widgets.push_back(w);
}

void Window::Draw(){
        clear();
        for(unsigned int i = 0; i < widgets.size(); i++) {
                (widgets[i])->Draw();
        }
}

void Window::Loop(){
	
        while (!stop) {
		Draw();
                int input = getch();
  //              int wx,wy,mx,my,bx,by=0;
    //            getmaxyx(stdscr,wy,wx); // boundaries of the screen
               
                        MEVENT event;

		 if(input == 'q') {
                        Stop();
                }

                else if(input == KEY_MOUSE) {
			if(getmouse(&event) == OK){

			if(event.bstate & BUTTON1_CLICKED){
			//getyx(stdscr, my,mx);
				
    for(unsigned int i = 0; i < widgets.size(); i++) {
  
int eventx = abs(event.x - widgets[i]->GetLeft());
int eventy = abs(event.y - widgets[i]->GetTop());
//cout << eventx << " " << eventy << " " << endl;  
    if(widgets[i]->Contains(event.x,event.y)) { 
	             (widgets[i])->OnMouseClick(eventx, eventy);
			}		
                        }
                 	}	
                	}
		}		
		refresh();     
   

	}
        endwin();
}

void Window::Stop(){
//        cerr << "Window Stop" << endl;
//        endwin();
stop = true;
}
