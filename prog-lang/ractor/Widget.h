// Widget.h
// abstarcted widget
/* Header for Widget (Widget.h) */
#ifndef WIDGET_H
#define WIDGET_H

#include "Rectangle.h"

class Widget : public Rectangle {

protected:
int l,r,t,b;   // proetcted data memebers
//int x,y;
//string text;

public:

Widget(int l, int r, int t, int b);   // Constructor
virtual ~Widget();

//extend Rectangle
int GetLeft() const;
int GetRight() const;
int GetTop() const;
int GetBottom() const;
bool Contains(int xc, int yc) const;

// virtual fucntion, run subclass version if overridden
virtual void Draw() = 0;   // pure virtual function
virtual void OnMouseClick(int x, int y); // virtual function

};

#endif
