// Rectangle.cpp
#include "Rectangle.h"

Rectangle::Rectangle(int l, int r, int t, int b){
this->l = l;
this->r = r;
this->t = t;
this->b = b;
}

int Rectangle::GetLeft() const {
        return this->l;

}
int Rectangle::GetRight() const {
        return this->r;
}

int Rectangle::GetTop() const {
        return this->t;
}

int Rectangle::GetBottom() const {
        return this->b;
}

void Rectangle::SetLeft(int l){
        this->l = l;
}

void Rectangle::SetRight(int r){
        this->r = r;
}

void Rectangle::SetTop(int t){
        this->t = t;
}

void Rectangle::SetBottom(int b){
        this->b = b;
}

int Rectangle::GetWidth() const {
        return (this->r - this->l)+1;
}

int Rectangle::GetHeight() const {
        return (this->b - this->t)+1;
}

bool Rectangle::Contains(int xc, int yc) const {

        //return (x >= this->l && x <= this->r && y >= this->t && y <= this->b);
        return (xc >= this->GetLeft() && xc <= this->GetRight() && yc >= this->GetTop() && yc <= this->GetBottom());
}

bool Rectangle::Intersects(const Rectangle& r){
        return (this->l == r.l || this->r == r.r || this->t == r.t ||this->b == b);
}
