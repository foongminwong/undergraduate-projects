// Slider.cpp
#include <functional>
#include "Slider.h"

Slider::Slider(int x, int y, int sliderMax, int sliderDefault):Widget(x,x+sliderMax,y,y){
      //  cerr << "initialzie slider" << endl;
        this->x = x;
        this->y = y;
        this->sliderMax = sliderMax;
        this->sliderDefault = sliderDefault;
}

int Slider::GetValue() const {
        return this->sliderDefault;
}

int Slider::GetMax() const {
        return this->sliderMax;
}

void Slider::Draw(){
    //    cerr << "slider draw" << endl;
        vector<string> slider(this->GetMax());
        string sliderText = ".";
        for(int i= 0; i < slider.size() ; i++) {
           	if(i == (this->GetValue())){
                        slider[i] = "o";
		}
                else{
                        slider[i] = ".";
		}
                
	sliderText.append(slider[i]);

        }
        mvprintw(this->y, this->x,sliderText.c_str());
}

void Slider::AddListener(std::function<void(int)> listener){
  //      cerr << "slider addlistener" << endl;
        listeners.push_back(listener);
}

void Slider::OnMouseClick(int c1, int c2){
//        cerr << "slider onmouseclick" << endl;
        this->sliderDefault = c1;
        for (unsigned int i = 0; i < listeners.size(); i++) {
                listeners[i](this->sliderDefault);
        }
}
