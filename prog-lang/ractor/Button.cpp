// Button.cpp

#include "Button.h"

Button::Button(int x, int y, const string & text):Widget(x,x+text.length()-1,y,y){
//        cerr << "initialize Button" << endl;
        this->x = x;
        this->y = y;
        this->text = text;
}

void Button::Draw(){
    //    cerr << "button draw" << endl;
	//	if(selected == true){
        attron(A_REVERSE);
        mvprintw(this->y, this->x, text.c_str());
        attroff(A_REVERSE);
		//else{

       // mvprintw(this->y, this->x, text.c_str());
//}

}

void Button::AddListener(std::function<void()> l){
      //  cerr << "button addlistener" << endl;
        listeners.push_back(l);

}

//pass the x- and y-coordinates of the click event in its coordinate system.
void Button::OnMouseClick(int x, int y){
        //cerr << "button onmouseclick" << endl;
       // selected = true;
	for (unsigned int i = 0; i < listeners.size(); i++) {
                listeners[i]();		
        }
}

//backup
//  std::function<void()> callback = this->AddListener;

//	std::function<void()> callback = std::function<void()>();
