// Rectangle.h

/* Header for Rectangle (Rectangle.h) */
#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <functional>
#include <iostream>
#include <ncurses.h>
#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
#include <string.h>

using namespace std;

class Rectangle {

protected:
int l,r,t,b;   // proetcted data memebers

public:
Rectangle(int l, int r, int t, int b);// Constructor

int GetLeft() const;
int GetRight() const;
int GetTop() const;
int GetBottom() const;

void SetLeft(int l);
void SetRight(int r);
void SetTop(int t);
void SetBottom(int b);

int GetWidth() const;
int GetHeight() const;

bool Contains(int xc, int yc) const;
bool Intersects(const Rectangle& r);
};

#endif
