// app.cpp

#include "Rectangle.h"
#include "Widget.h"
#include "Button.h"
#include "Checkbox.h"
#include "Label.h"
#include "List.h"
#include "Slider.h"
#include "Window.h"
int main(){

	Window w;
	start_color();
    	init_pair(1, COLOR_MAGENTA, COLOR_BLACK);
	
	Label *l = new Label (5,0, "++++++++++++++++++++++++++ SelfChecker ++++++++++++++++++++++");
 	w.Add(l);       
	Checkbox *box1 = new Checkbox(10, 3, "echoargs");
	Checkbox *box2 = new Checkbox(10, 4, "funfun");
        Checkbox *box3 = new Checkbox(10, 5, "ractor");
        Checkbox *box4 = new Checkbox(10, 6, "regexercise");
        Checkbox *box5 = new Checkbox(10, 7, "terp");
        Checkbox *box6 = new Checkbox(10, 8, "wasd");
	attron(COLOR_PAIR(1));
	w.Add(box1);
	w.Add(box2);
	w.Add(box3);
	w.Add(box4);
	w.Add(box5);
	w.Add(box6);

Button *b = new Button(25, 3, "Grade");
  
  w.Add(b);
Label *l2 = new Label (7,10,"Not all assignment passed.");
w.Add(l2);

b->AddListener([&]() {
    l2->SetText("6 out of 6 homework passed! You get a final exam waiver!");
    
  });
	w.Loop();	
}
/*
Window w;
  int nclicks = 0;

  vector<int> selected;
  vector<string> options {"Wisconsin", "Minnesota", "Iowa"};
  List *state = new List(1, 8, options);
  Label *l = new Label(0, 0, "Select Wisconsin...");
  Button *b = new Button(25, 3, "Quit");
  w.Add(state);
  w.Add(l);
  w.Add(b);

  Checkbox *box1 = new Checkbox(5, 20, "animal");
  Checkbox *box2 = new Checkbox(5, 21, "vegetable");
  box2->IsChecked(true);
  Checkbox *box3 = new Checkbox(5, 22, "mineral");
  w.Add(box1);
  w.Add(box2);
  w.Add(box3);

  Slider *s = new Slider(7, 12, 7, 4);
  w.Add(s);

  box1->AddListener([&](bool is) {
    ++nclicks;
    selected.push_back(100 + (is ? 10 : 0));
    if (nclicks == 5) {
      l->SetText("Now uncheck the vegetable checkbox...");
    }
  });
  box2->AddListener([&](bool is) {
    ++nclicks;
    selected.push_back(101 + (is ? 10 : 0));
    if (nclicks == 6) {
      l->SetText("Now check the vegetable checkbox...");
    } else if (nclicks == 7) {
      l->SetText("Now check the mineral checkbox...");
    }
  });
  box3->AddListener([&](bool is) {
    ++nclicks;
    selected.push_back(102 + (is ? 10 : 0));
    if (nclicks == 8) {
      l->SetText("Now set the slider to 0...");
    }
  });

  state->AddListener([&](int i) {
    selected.push_back(i); 
    ++nclicks;
    if (nclicks == 1) {
      l->SetText("Now select Iowa...");
    } else if (nclicks == 2) {
      l->SetText("Now select Minnesota...");
    } else if (nclicks == 3) {
      l->SetText("Now deselect Minnesota...");
    } else if (nclicks == 4) {
      l->SetText("Now check the animal checkbox...");
    }
  });

  s->AddListener([&](int i) {
    ++nclicks;
    selected.push_back(i + 1000);
    if (nclicks == 9) {
      l->SetText("Now set the slider to its maximum value...");
    } else if (nclicks == 10) {
      l->SetText("Now click Quit...");
    }
  });

  b->AddListener([&]() {
    selected.push_back(300);
    w.Stop();
  });

  w.Loop();




*/



	
//    init_pair(1, COLOR_BLACK, COLOR_RED);
  //  init_pair(2, COLOR_BLACK, COLOR_GREEN);

  //  attron(COLOR_PAIR(1));
    //printw("This should be printed in black with a red background!\n");

    //attron(COLOR_PAIR(2));
    //printw("And this in a green background!\n");	

