module MindReader where
import Data.List(foldl', scanl', group)
import GHC.Exts(sortWith)
import Data.Bits(testBit)
import System.Random.Shuffle(shuffleM)
import System.Environment(getArgs)
import Numeric (showHex, showIntAtBase)
import Data.Char (intToDigit)
import Data.Char (digitToInt)
import Control.Monad
import Control.Monad.Reader

ff :: (Show a) => [a]-> [String]
ff = (>>= return.show)

-- reverse
-- `elem`
-- filter <= 10 [1..81]

toBinary :: Int -> String
toBinary x = showIntAtBase 2 intToDigit x "";

-- map toBinary [1..90]
-- filter ((>=2).length) x
-- map read x :: [Int] 
-- length (x!!3) - 2

hasOne :: Int -> String -> Bool
hasOne i x = (x !!((length x)-i-1)) == '1'
 

hhasOne :: String -> Bool
hhasOne x = (x !!((length x)-1)) == '1'

intsWithBit :: Int -> Int -> [Int]
--intsWithBit max i = map binaryToDecimal(filter ((>=i+1).length)(map toBinary [1..max]))
--intsWithBit max i = map read (map toBinary [1..max]) :: [Int]
--intsWithBit max i = map binaryToDecimal(filter hasOne (filter ((>=i+1).length)(map toBinary [1..max])))
intsWithBit max i = map binaryToDecimal(filter (hasOne i)(filter ((>=i+1).length)(map toBinary [1..max])))
--intsWithBit max i = map read (map toBinary [1..max]) :: [Int]
--intsWithBit fil num = map read (map toBinary [1..num]) :: Int

binaryToDecimal :: String -> Int
binaryToDecimal = foldl' (\a x -> a * 2 + digitToInt x) 0

dTB2 :: Int -> [Int]
dTB2 x = reverse $ dTB2' x
  where
    dTB2' 0 = []
    dTB2' y = let (a,b) = quotRem y 2 in [b] ++ dTB2' a

decimalToBinary' :: Int -> String
decimalToBinary' x = reverse((concat(ff (dTB2 x))))

decimalToBinary :: Int -> String
decimalToBinary 0 = "0"
decimalToBinary x = reverse(decimalToBinary' x)

decimalToBinary2 :: Int -> String
decimalToBinary2 x = concat(ff(dTB2 x))

nbits :: Int -> Int
nbits 0 = 1
nbits x = length (decimalToBinary2 x)

promptForBit :: Int -> Int -> IO Int
promptForBit max i = do
	putStrLn "" 	
	putStrLn (show (intsWithBit max i))
	putStr "Is your number in this list? y/[n]? "
	input <- getLine
	if(input == "y") then return 1 else return 0

main :: IO()
main = do 
	putStr "What's your favorite upper bound? "
	bound <- getLine
	putStrLn ""
	putStrLn $ "Think of a number in [1, " ++ bound ++ "]. But don't tell me. I will read your mind!"
	let i = nbits(read bound::Int) 
	    iBound = (read bound::Int)
	--putStrLn $ show i
	--putStrLn $ show iBound
	k <- mapM (promptForBit (read bound::Int)) [0..nbits(read bound::Int)-1]
	--putStrLn $ show k
	let bS = reverse(concat (ff k))
	--putStrLn bS
	putStrLn ""
	putStrLn $ "Your number is " ++  (show(binaryToDecimal(bS)) ++ ".")   
	return()

